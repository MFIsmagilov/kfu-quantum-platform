package kpfu.magistracy.controller.execution.commands;

import com.google.gson.annotations.SerializedName;

/**
 * first 3 commands constitute the universal basis
 */
public enum CommandTypes {

    @SerializedName("CQET")
    CQET,
    @SerializedName("QET")
    QET,
    @SerializedName("PHASE")
    PHASE,

    //для реализации быстрого CCNOT
    //аккуратно переставляй потому что на функция isSubCQET зависит от их порядка
    @SerializedName("CQET000")
    CQET000,

    @SerializedName("CQET010")
    CQET010,

    @SerializedName("CQET100")
    CQET100,

    @SerializedName("CQET110")
    CQET110,


    //для модифицированного CNOT где у контролирующуего логического кубита задействуется нижний физический кубит
    @SerializedName("CQET1")
    CQET1,

    MEASURE,

    INIT;


    public boolean isCQET(){
        return this == CommandTypes.CQET || this == CommandTypes.CQET1;
    }

    public boolean isSubCQET() {
        return this.ordinal() > 2 && this.ordinal() < 7;
    }

}