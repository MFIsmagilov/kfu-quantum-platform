package kpfu.magistracy.service_for_controller;

import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient;

import java.util.LinkedHashMap;

/**
 * Created by maratismagilov on 05.05.2018.
 */
public interface ResultCallback{
    void finishing(OwnerData ownerData, LinkedHashMap<LogicalQubitAddressFromClient, Integer> result);
}