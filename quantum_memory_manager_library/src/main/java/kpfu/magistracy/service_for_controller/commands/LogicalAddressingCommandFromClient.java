package kpfu.magistracy.service_for_controller.commands;

import com.google.gson.annotations.SerializedName;
import kpfu.magistracy.controller.execution.commands.CommandTypes;
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient;


public class LogicalAddressingCommandFromClient {
    //command name from universal basis
    @SerializedName("command_name")
    private CommandTypes mCommandType;
    @SerializedName("command_param")
    private Double mCommandParam;
    @SerializedName("qubit_1")
    private LogicalQubitAddressFromClient mQubit_1;
    @SerializedName("qubit_2")
    private LogicalQubitAddressFromClient mQubit_2;

    @SerializedName("qubit_3")
    private LogicalQubitAddressFromClient mQubit_3;


    public LogicalAddressingCommandFromClient(CommandTypes mCommandType, Double mCommandParam, LogicalQubitAddressFromClient qubit_1, LogicalQubitAddressFromClient qubit_2, LogicalQubitAddressFromClient qubit_3) {
        this.mCommandType = mCommandType;
        this.mCommandParam = mCommandParam;
        mQubit_1 = qubit_1;
        mQubit_2 = qubit_2;
        mQubit_3 = qubit_3;
    }

    public LogicalAddressingCommandFromClient(CommandTypes commandType, Double commandParam,
                                              LogicalQubitAddressFromClient qubit_1,
                                              LogicalQubitAddressFromClient qubit_2) {
        mCommandType = commandType;
        mCommandParam = commandParam;
        mQubit_1 = qubit_1;
        mQubit_2 = qubit_2;
    }

    public LogicalAddressingCommandFromClient(CommandTypes commandType, Double commandParam,
                                              LogicalQubitAddressFromClient qubit_1) {
        mCommandType = commandType;
        mCommandParam = commandParam;
        mQubit_1 = qubit_1;
    }

    public CommandTypes getCommandType() {
        return mCommandType;
    }

    public Double getCommandParam() {
        return mCommandParam;
    }

    public LogicalQubitAddressFromClient getQubit_1() {
        return mQubit_1;
    }

    public LogicalQubitAddressFromClient getQubit_2() {
        return mQubit_2;
    }

    public LogicalQubitAddressFromClient getQubit_3() {
        return mQubit_3;
    }
}
