package kpfu.quantum.server

inline fun <reified T> array2dOf(sizeOuter: Int, sizeT: Int, noinline TInit: (Int) -> T): Array<Array<T>> = Array(sizeOuter) { Array<T>(sizeT, TInit) }

inline fun <reified T> arrayOf(sizeT: Int, noinline TInit: (Int) -> T): Array<T> = Array<T>(sizeT, TInit)
