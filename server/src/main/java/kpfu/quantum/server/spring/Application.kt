package kpfu.quantum.server.spring

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync


/**
 * Created by maratismagilov on 05.05.2018.
 */
@Configuration
//@EnableAutoConfiguration
//@ComponentScan
@EnableAsync
@SpringBootApplication
class Application{
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val ctx = SpringApplication.run(Application::class.java, *args)
        }
    }
}