package kpfu.quantum.server.spring

import kpfu.magistracy.service_for_controller.ResultCallback
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.spring.models.QVMInit
import kpfu.quantum.server.spring.models.Result
import kpfu.quantum.server.translator.clientsdata.*
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.concurrent.CompletableFuture
import kotlin.collections.HashMap

/**
 * Created by maratismagilov on 05.05.2018.
 */
@RestController
//@EnableWebSocket
@RequestMapping("v1/qvm")
class QvmController {


    private val log = LoggerFactory.getLogger(QvmController::class.java)

    @RequestMapping("/default_settings_emulator")
    fun getDefaultSettingsEmuator(): QVMInit {
        return QVMInit(200.0, 50.0, 50.0, 3)
    }


    @RequestMapping("/execute_program", method = [(RequestMethod.POST)])
    fun executeProgramm(
            @RequestHeader("X-User-Id") userId: String,
            @RequestHeader("X-Api-Key") apiKey: String,
            @RequestBody highQuantumProgramm: HighQubitQuantumProgram): CompletableFuture<MutableList<Result>>? {

        if (userId != "quantum" && apiKey != "&<C]E3z8L~yjw&D") {
            throw Exception("Неправильный userId или apiKey")
        }

        return CompletableFuture
                .supplyAsync {
                    val quantumProgramm = QubitQuantumProgram(highQuantumProgramm, User(userId))
                    log.info(quantumProgramm.toString())
                    var resultFromCallback: LinkedHashMap<LogicalQubitAddressFromClient, Int>? = null
                    quantumProgramm.execute(
                            ResultCallback { ownerData, result ->
                                run {
                                    log.info("RESULT: $result")
                                    resultFromCallback = result
                                }
                            }
                    )
                    //todo: Едить колотить!
                    while (resultFromCallback == null) Thread.sleep(100)
                    val resultList = mutableListOf<Result>()
                    resultFromCallback?.forEach { key, value ->
                        resultList.add(Result(key, value))
                    }
                    return@supplyAsync resultList
                }
                .thenApply {
                    return@thenApply it
                }


    }


    @RequestMapping("/execute_qudit_program", method = [(RequestMethod.POST)])
    fun executeProgramWithQudit(
            @RequestBody highQuantumProgramm: HighQuditQuantumProgram
    ): CompletableFuture<HashMap<Int, Int>>? {

        return CompletableFuture.supplyAsync {
            val program = QuditQuantumProgram(highQuantumProgramm)

            println("!executeProgramWithQudit")
            program.print()
            val result = program.execute()
            return@supplyAsync result
        }
    }

}
