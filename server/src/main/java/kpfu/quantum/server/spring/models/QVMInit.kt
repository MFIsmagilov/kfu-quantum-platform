package kpfu.quantum.server.spring.models

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class QVMInit constructor(
        val maxMemoryFrequency: Double,
        val minMemoryFrequency: Double,
        val memoryTimeCycle: Double,
        val processingUnitsCount: Int
)