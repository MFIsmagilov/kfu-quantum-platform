package kpfu.quantum.server.spring.models

import com.fasterxml.jackson.annotation.JsonInclude
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Result constructor(
        val logicalQubitAddressFromClient: LogicalQubitAddressFromClient,
        val value: Int
)