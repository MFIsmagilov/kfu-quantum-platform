package kpfu.quantum.server.translator

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.SingleQubitArbitaryOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.*
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import org.apache.commons.math3.complex.Complex

/**
 * Created by maratismagilov on 05.05.2018.
 */
class HightLevelOperatorFactory(
        val highLevelOperatorName: OperatorName,
        val qubits: List<Qubit>,
        val params: Params? = null,
        val matrix: Array<Array<Complex>>? = null) {

    private fun creator(qubits: List<Qubit>, params: Params, matrix: Array<Array<Complex>>): HighLevelOperator {
        return when (highLevelOperatorName) {
            OperatorName.HADAMAR -> Hadamar(qubits[0])
            OperatorName.T -> T(qubits[0])
            OperatorName.THerm -> THerm(qubits[0])
            OperatorName.S -> S(qubits[0])
            OperatorName.CNOT -> CNOT(qubits[0], qubits[1])
            OperatorName.CCNOT -> CCNOT(qubits[0], qubits[1], qubits[2])
            OperatorName.U1 -> U1(params, qubits)
            OperatorName.U2 -> U2(params, qubits)
            OperatorName.U3 -> U3(params, qubits)
            OperatorName.Rx -> Rx(params, qubits[0])
            OperatorName.Ry -> Ry(params, qubits[0])
            OperatorName.Rz -> Rx(params, qubits[0])
            OperatorName.SingleQubitArbitaryOperator -> SingleQubitArbitaryOperator(matrix, qubits[0])
            else -> {
                throw Exception("Неизвестный стандартный оператор")
            }
        }
    }

    fun buildHighOperator(): HighLevelOperator {
        return creator(qubits, params ?: Params.empty(), matrix ?: emptyArray())
    }
}