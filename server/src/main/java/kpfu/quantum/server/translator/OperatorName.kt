package kpfu.quantum.server.translator

/**
 * Created by maratismagilov on 05.05.2018.
 */
//**кажется** такой enum я вводил
//для простоты поиска в json
//в json приходит ключ в ввиде имени оператора
//чтобы не хардкотить все имена перечислены в одном месте
enum class OperatorName(val highLevelOperatorName: String) {
    HADAMAR("Hadamard"),
    T("T"),
    THerm("THerm"),
    S("S"),
    CNOT("CNOT"),
    CCNOT("CCNOT"),
    X("X"),
    U1("U1"),
    U2("U2"),
    U3("U3"),
    Rx("Rx"),
    Ry("Ry"),
    Rz("Rz"),
    SingleQubitArbitaryOperator("SingleQubitArbitaryOperator"), //user friendly operator
    MultiQubitArbitaryOperator("MultiQubitArbitaryOperator"), //user friendly operator


    //operators by qudits


    Init("qudit_init"),

    ApplyF0("qudit_applyf0"),
    ApplyZ("applyZ"),
    ApplyZConj("applyZconjugate"),

    ApplyX("applyX"),
    ApplyXConj("applyXconjugate"),

    Measure("measure"),


    TWEKABLEOPERATOR("TWEKABLEOPERATOR"), // этот оператор не виден пользователю
    UNNAMED("UNNAMED"); // оператор который получим при сложении двух

    companion object {
        fun from(highLevelOperatorName: String) = OperatorName.values().first { it.highLevelOperatorName == highLevelOperatorName }
    }
}