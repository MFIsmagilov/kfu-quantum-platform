package kpfu.quantum.server.translator

import kpfu.quantum.server.translator.highleveloperators.qudit.*

class QuditOperatorFactory(val operatorName: OperatorName,
                           val qudits: List<Long>,
                           val idxCMem: Long?,
                           val dim: Long?,
                           val nreg: Long?,
                           val i: Long?,
                           val x: Double?,
                           val y: Double?,
                           val tau: Double?
) {

    fun buildHighOperator(): QuditOperator {

        val idxQMem0 = qudits[0]
        //fixme опасненько
        return when (operatorName) {
            OperatorName.ApplyX -> ApplyXWrap(idxQMem0, i!!, x!!, y!!)
            OperatorName.ApplyXConj -> ApplyXConjWrap(idxQMem0, i!!, x!!, y!!)
            OperatorName.ApplyZConj -> ApplyZConjWrap(idxQMem0, i!!, tau!!)
            OperatorName.ApplyZ -> ApplyZWrap(idxQMem0, i!!, tau!!)
            OperatorName.Init -> Init(nreg!!, dim!!)
            OperatorName.Measure -> MeasureWrap(idxQMem0.toInt(), idxCMem!!.toInt())
            else -> throw IllegalStateException("Оператор не определен.")
        }
    }
}