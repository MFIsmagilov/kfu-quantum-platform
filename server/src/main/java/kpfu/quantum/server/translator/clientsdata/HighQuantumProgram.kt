package kpfu.quantum.server.translator.clientsdata

import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.AbstractLogicalAddressingCommandFromClient

open class HighQuantumProgram<T: HighLevelOperator>(
        var highOperators: List<T>
) {
    fun getAllCommands(): MutableList<AbstractLogicalAddressingCommandFromClient> {
        val allCommands = mutableListOf<AbstractLogicalAddressingCommandFromClient>()
        highOperators.forEach {
            allCommands.addAll(it.getLowOperator().commands)
        }
        return allCommands
    }
}