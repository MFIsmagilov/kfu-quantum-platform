package kpfu.quantum.server.translator.clientsdata

import com.fasterxml.jackson.annotation.JsonProperty
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.QubitOperator


class HighQubitQuantumProgram(
        val countQubits: Int,

        @JsonProperty("circuit")
        operators: List<QubitOperator>) : HighQuantumProgram<HighLevelOperator>(operators)