package kpfu.quantum.server.translator.clientsdata

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import kpfu.quantum.server.translator.clientsdata.deserializers.HighQuantumQuditProgramDeserializer
import kpfu.quantum.server.translator.highleveloperators.qudit.QuditOperator

@JsonDeserialize(using = HighQuantumQuditProgramDeserializer::class)
class HighQuditQuantumProgram(
        val quditCount: Long,
        val dim: Long,
        highOperators: List<QuditOperator>) :
        HighQuantumProgram<QuditOperator>(highOperators = highOperators)