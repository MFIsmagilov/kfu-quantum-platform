package kpfu.quantum.server.translator.clientsdata

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Created by maratismagilov on 07.05.2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Qubit(
        val value: Int,
        override val address: Int
) : UnitInformation {

    companion object {

        fun create(address: Int): Qubit {
            return Qubit(0, address)
        }

    }
}