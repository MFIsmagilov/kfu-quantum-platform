package kpfu.quantum.server.translator.clientsdata

import com.google.gson.GsonBuilder
import kpfu.magistracy.service_for_controller.ResultCallback
import kpfu.magistracy.service_for_controller.ServiceManager
import kpfu.magistracy.service_for_controller.commands.CommandsFromClientDTO
import kpfu.magistracy.service_for_controller.commands.LogicalAddressingCommandFromClient

/**
 * Created by maratismagilov on 05.05.2018.
 */
class QubitQuantumProgram(val highQuantumProgramm: HighQubitQuantumProgram, val user: User) {
    private var commandsFromClientDTO: CommandsFromClientDTO = CommandsFromClientDTO()
    private var serviceManager: ServiceManager = ServiceManager.getServiceManager()

    init {
        //fixme if qubit_emul else qudit_emul
        commandsFromClientDTO.qubitCount = highQuantumProgramm.countQubits
        commandsFromClientDTO.logicalAddressingCommandFromClientList = highQuantumProgramm.getAllCommands().map {
            //жестко
            GsonBuilder().create().fromJson(it.toJson(), LogicalAddressingCommandFromClient::class.java)
        }
    }

    fun execute(result: ResultCallback) {
        serviceManager.putCommandsToExecutionQueue(user.tokenValue, GsonBuilder().create().toJson(commandsFromClientDTO), result)
    }
}