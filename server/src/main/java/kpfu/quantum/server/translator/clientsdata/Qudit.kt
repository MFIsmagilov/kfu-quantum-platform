package kpfu.quantum.server.translator.clientsdata

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Qudit(
        override val address: Int //idxCMem
) : UnitInformation