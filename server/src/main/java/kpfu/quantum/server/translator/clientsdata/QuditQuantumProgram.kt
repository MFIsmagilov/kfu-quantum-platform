package kpfu.quantum.server.translator.clientsdata

import kmqc.manager.instruction.QInstruction
import kpfu.quantum.server.translator.highleveloperators.qudit.MeasureWrap
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

//ввел эту обертку чтобы было аналогично с Qubit
class QuditQuantumProgram(val highQuditQuantumProgram: HighQuditQuantumProgram) {
    fun execute(): HashMap<Int, Int> {
        val result = hashMapOf<Int, Int>()
        highQuditQuantumProgram.highOperators.forEach { highOperator ->

            (highOperator.getLowOperator() as QuditLowLevelOperator).apply {
                println("Execute: ${this}")
            }

            (highOperator.getLowOperator() as QuditLowLevelOperator).execute()
            if (highOperator is MeasureWrap) {
                result[highOperator.idxCMem] = QInstruction.getIdxCMem(highOperator.idxCMem)
            }
        }
        return result
    }

    fun print() {
        highQuditQuantumProgram.highOperators.forEach { highOperator ->
            println(highOperator)
        }
    }
}