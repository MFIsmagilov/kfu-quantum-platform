package kpfu.quantum.server.translator.clientsdata

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Created by maratismagilov on 05.05.2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
data class User(
        //todo: rename
        val tokenValue: String
)