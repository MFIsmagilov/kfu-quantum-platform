package kpfu.quantum.server.translator.clientsdata.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import kpfu.quantum.server.translator.clientsdata.HighQuditQuantumProgram
import kpfu.quantum.server.translator.highleveloperators.qudit.Init
import kpfu.quantum.server.translator.highleveloperators.qudit.QuditOperator

/*
    Решили избавиться от оператора init поэтому парсим ручками все
 */
class HighQuantumQuditProgramDeserializer : JsonDeserializer<HighQuditQuantumProgram>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): HighQuditQuantumProgram {
        if (p != null) {
            val node = p.readValueAsTree<JsonNode>()
            val quditCount = node.get("count_qudits").longValue()
            val dimQudit = node.get("dimension").longValue()
            val circuit = node.get("circuit")

            return HighQuditQuantumProgram(quditCount = quditCount,
                    dim = dimQudit,
                    highOperators = deserializeCircuit(circuit).apply {
                        add(0, Init(quditCount, dimQudit))
                    }
            )
        }
        throw Exception("Что-то пошло не так, где JsonParser")
    }


    fun deserializeCircuit(node: JsonNode): ArrayList<QuditOperator> {
        if (node.isArray) {
            val deserializer = QuditOperatorDeserializer()
            val operators = node.map { operator -> deserializer.deserialize(operator) }
            return ArrayList(operators)
        }
        throw IllegalStateException("circuit must be array")
    }
}