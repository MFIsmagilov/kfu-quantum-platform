package kpfu.quantum.server.translator.clientsdata.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kpfu.quantum.server.translator.HightLevelOperatorFactory
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import org.apache.commons.math3.complex.Complex

class QubitOperatorDeserializer : JsonDeserializer<HighLevelOperator>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): HighLevelOperator {
        if (p != null) {
            val oc = p.codec
            val node = oc.readTree<JsonNode>(p)
            val paramsNode = node.get("params")
            var params: Params? = if (paramsNode != null) {
                jacksonObjectMapper().readValue(paramsNode.toString(), Params::class.java)
            } else {
                null
            }
            val matrixNode = node.get("matrix")
            val matrix = if (matrixNode != null) {
                /**
                 * complex matrix :
                 * [
                 *  [[1, 2], [3, 4]], //first row
                 *  [[5, 6], [7, 8]]  // всегда 2x2 - Real и Imaginary
                 * ]
                 */
//                val doubleMatrix: Array<Array<Array<Double>>> = jacksonObjectMapper().readValue(matrixNode.textValue(), Array<Array<Array<Double>>>::class.java)
                val doubleMatrix: JsonNode = matrixNode///jacksonObjectMapper().readValue(matrixNode.textValue(), Array<Array<Array<Double>>>::class.java)
                val result: Array<Array<Complex>> = kotlin.run {
                    val complexMatrix = mutableListOf<Array<Complex>>()
                    doubleMatrix.forEach { row ->
                        run {
                            val complexRow = mutableListOf<Complex>()
                            row.forEach {
                                val complex = Complex(it[0].doubleValue(), it[1].doubleValue())
                                complexRow.add(complex)
                            }
                            complexMatrix.add(complexRow.toTypedArray())
                        }
                    }
                    complexMatrix.toTypedArray()
                }
                result
            } else {
                null
            }
            val operatorName = node.get("operator").textValue()
            val qubitsNode = node.get("qubits")//.toString(), Qubit::class.java)
            val listQubits = mutableListOf<Qubit>()
            qubitsNode.forEach {
                val qubit = Qubit(0, it.asInt())
                listQubits.add(qubit)
            }
            return HightLevelOperatorFactory(
                    highLevelOperatorName = OperatorName.from(operatorName),
                    qubits = listQubits,
                    params = params,
                    matrix = matrix).buildHighOperator()
        }
        throw Exception("Что-то пошло не так, где JsonParser")
    }
}