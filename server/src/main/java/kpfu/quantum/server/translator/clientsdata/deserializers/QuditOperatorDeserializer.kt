package kpfu.quantum.server.translator.clientsdata.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.QuditOperatorFactory
import kpfu.quantum.server.translator.highleveloperators.qudit.QuditOperator

class QuditOperatorDeserializer : JsonDeserializer<QuditOperator>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): QuditOperator {
        if (p != null) {
            val node = p.readValueAsTree<JsonNode>()
            return deserialize(node)
        }
        throw Exception("Что-то пошло не так, где JsonParser")
    }

    fun deserialize(node: JsonNode): QuditOperator {
        val params = node.get("params")
        val qudits = node.get("qudits").map { it.longValue() }

        return QuditOperatorFactory(
                operatorName = OperatorName.from(node.get("operator").textValue()),
                qudits = qudits,
                idxCMem = params.get("idx_creg")?.longValue(),
                dim = params.get("dim")?.longValue(),
                nreg = params.get("nreg")?.longValue(),
                i = params.get("i")?.longValue(),
                x = params.get("x")?.doubleValue(),
                y = params.get("y")?.doubleValue(),
                tau = params.get("theta")?.doubleValue()
        ).buildHighOperator()
    }
}