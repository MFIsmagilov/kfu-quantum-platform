package kpfu.quantum.server.translator.exceptions

/**
 * Created by maratismagilov on 05.08.2018.
 */
class MuParamNotFound : Exception("Parameter MU not found")

class PhiParamNotFound : Exception("Parameter Phi not found")

class ThetaParamNotFound : Exception("Parameter Theta not found")