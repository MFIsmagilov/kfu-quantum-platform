package kpfu.quantum.server.translator.highleveloperators

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.UnitInformation
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

/**
 * Created by maratismagilov on 05.05.2018.
 */

abstract class HighLevelOperator(
        open val operatorName: OperatorName,
        open var unitsInformation: List<UnitInformation>,
        var params: Params? = null
) {
    abstract fun getLowOperator(): LowLevelOperator
}

