package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

/**
 * Created by maratismagilov on 22.05.2018.
 */
//Тоффоли
class CCNOT(qubit1: Qubit, qubit2: Qubit, qubit3: Qubit) :
        QubitOperator(OperatorName.CCNOT, listOf(qubit1, qubit2, qubit3)) {
    override fun getLowOperator(): LowLevelOperator {
        val q1 = unitsInformation[0] as Qubit
        val q2 = unitsInformation[1] as Qubit
        val q3 = unitsInformation[2] as Qubit

        return Hadamar(q3).getLowOperator() *
                CNOT(q2, q3).getLowOperator() *
                THerm(q3).getLowOperator() *
                CNOT(q1, q3).getLowOperator() *
                T(q3).getLowOperator() *
                CNOT(q2, q3).getLowOperator() *
                THerm(q3).getLowOperator() *
                CNOT(q1, q3).getLowOperator() *
                T(q3).getLowOperator() *
                Hadamar(q3).getLowOperator() *
                THerm(q2).getLowOperator() *
                CNOT(q1, q2).getLowOperator() *
                THerm(q2).getLowOperator() *
                CNOT(q1, q2).getLowOperator() *
                S(q2).getLowOperator() *
                T(q1).getLowOperator()
    }

    override fun toString(): String {
        return "CCNOT ${unitsInformation[0].address} ${unitsInformation[1].address} ${unitsInformation[2].address}"
    }
}