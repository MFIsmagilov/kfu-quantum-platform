package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qubit.QubitLowLevelOperator

/**
 * Created by maratismagilov on 22.05.2018.
 */
class CNOT(qubit1: Qubit, qubit2: Qubit) : QubitOperator(OperatorName.CNOT, listOf(qubit1, qubit2)) {
    override fun getLowOperator(): LowLevelOperator {
        val q1 = unitsInformation[0].address
        val q2 = unitsInformation[1].address
//        return LowLevelOperator(mutableListOf(
//                LogicalAddressingCommandFromClient(
//                        CommandTypes.CQET, Math.PI , LogicalQubitAddressFromClient(q1),
//                        LogicalQubitAddressFromClient(q2)),
//                LogicalAddressingCommandFromClient(
//                        CommandTypes.PHASE, Math.PI / 4, LogicalQubitAddressFromClient(q1))
//        ))
        return QubitLowLevelOperator(mutableListOf(
                LogicalAddressingCommandFromClient(
                        CommandTypes.QET, Math.PI, LogicalQubitAddressFromClient(q1)
                ),
                LogicalAddressingCommandFromClient(
                        CommandTypes.CQET, Math.PI, LogicalQubitAddressFromClient(q1), LogicalQubitAddressFromClient(q2)
                ),
                LogicalAddressingCommandFromClient(
                        CommandTypes.PHASE, Math.PI / 2, LogicalQubitAddressFromClient(q1)
                ),
                LogicalAddressingCommandFromClient(
                        CommandTypes.QET, Math.PI, LogicalQubitAddressFromClient(q1)
                )
        ))
    }

    override fun toString(): String {
        return "CNOT ${unitsInformation[0].address} ${unitsInformation[1].address}"
    }
}