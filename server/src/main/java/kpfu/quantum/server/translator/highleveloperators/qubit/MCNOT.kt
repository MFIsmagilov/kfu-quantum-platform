package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.tweakmathmodel.MCCNOT
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qubit.QubitLowLevelOperator

//modified CNOT
class MCNOT(qubit1: Qubit, qubit2: Qubit) : QubitOperator(OperatorName.TWEKABLEOPERATOR, listOf(qubit1, qubit2)) {
    override fun getLowOperator(): LowLevelOperator {
        val q1 = unitsInformation[0] as Qubit
        val q2 = unitsInformation[1] as Qubit
        val op = MCCNOT(q1, q2, q2, true, true).getLowOperator()
        op.commands.add(
                LogicalAddressingCommandFromClient(
                        CommandTypes.PHASE, Math.PI / 2, LogicalQubitAddressFromClient(q1.address)
                )
        )
        return op
    }

    override fun toString(): String {
        return "CNOT ${unitsInformation[0].address} ${unitsInformation[1].address}"
    }
}