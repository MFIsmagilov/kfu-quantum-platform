package kpfu.quantum.server.translator.highleveloperators.qubit

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.clientsdata.deserializers.QubitOperatorDeserializer
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params

@JsonDeserialize(using = QubitOperatorDeserializer::class)
abstract class QubitOperator(operatorName:OperatorName, qubits: List<Qubit>, params: Params? = null):
        HighLevelOperator(operatorName, qubits, params){
    val mu = params?.mu
    val phi = params?.phi
    val theta = params?.theta
}