package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.exceptions.ThetaParamNotFound
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

/**
 * Created by maratismagilov on 05.08.2018.
 */

class Ry(params: Params, qubit: Qubit) : QubitOperator(
        params = params,
        qubits = listOf(qubit),
        operatorName = OperatorName.Ry) {
    override fun getLowOperator(): LowLevelOperator {
        if (theta == null) throw ThetaParamNotFound()

        val q0 = unitsInformation[0] as Qubit

        val rz1 = Rz(Params(theta = null, phi = Math.PI / 2, mu = null), q0).getLowOperator()
        val rx = Rx(Params(theta = null, phi = null, mu = theta), q0).getLowOperator()
        val rz2 = Rz(Params(theta = null, phi = -Math.PI / 2, mu = null), q0).getLowOperator()
        return rz1 * rx * rz2
    }

    override fun toString(): String {
        return "Ry ${unitsInformation[0].address}"
    }
}