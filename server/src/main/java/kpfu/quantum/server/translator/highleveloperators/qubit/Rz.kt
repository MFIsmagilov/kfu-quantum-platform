package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.exceptions.PhiParamNotFound
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qubit.QubitLowLevelOperator

/**
 * Created by maratismagilov on 05.08.2018.
 */
class Rz(params: Params, qubit: Qubit) : QubitOperator(
        operatorName = OperatorName.Rx,
        qubits = listOf(qubit),
        params = params) {
    override fun getLowOperator(): LowLevelOperator {
        if (phi == null) throw PhiParamNotFound()

        val q0 = unitsInformation[0].address
        return QubitLowLevelOperator(mutableListOf(
                LogicalAddressingCommandFromClient(
                        CommandTypes.PHASE, phi, LogicalQubitAddressFromClient(q0))
        ))
    }

    override fun toString(): String {
        return "Rz ${unitsInformation[0].address}"
    }
}