package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qubit.QubitLowLevelOperator

/**
 * Created by maratismagilov on 22.05.2018.
 */
class THerm(qubit: Qubit): QubitOperator(OperatorName.THerm, listOf(qubit)) {
    override fun getLowOperator(): LowLevelOperator {
        val address = unitsInformation[0].address
        return QubitLowLevelOperator(mutableListOf(
                LogicalAddressingCommandFromClient(
                        CommandTypes.PHASE, -Math.PI / 4, LogicalQubitAddressFromClient(address))
        ))
    }
}