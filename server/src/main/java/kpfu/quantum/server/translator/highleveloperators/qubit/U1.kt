package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.*
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.exceptions.MuParamNotFound
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

/**
 * Created by maratismagilov on 05.08.2018.
 */
class U1(params: Params,
         qubits: List<Qubit>
) : QubitOperator(operatorName = OperatorName.U1,
        qubits = qubits,
        params = params) {

    override fun getLowOperator(): LowLevelOperator {
        if (mu == null) throw MuParamNotFound()
        val prms = params ?: throw IllegalArgumentException("Params must not be null")
        return Rz(prms, unitsInformation[0] as Qubit).getLowOperator()
    }
}