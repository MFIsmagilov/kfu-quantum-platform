package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.exceptions.MuParamNotFound
import kpfu.quantum.server.translator.exceptions.PhiParamNotFound
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

/**
 * Created by maratismagilov on 05.08.2018.
 */
class U2(params: Params,
         qubits: List<Qubit>
) : QubitOperator(operatorName = OperatorName.U2,
        qubits = qubits,
        params = params) {

    override fun getLowOperator(): LowLevelOperator {

        if (mu == null) throw MuParamNotFound()

        if (phi == null) throw  PhiParamNotFound()

        val address = unitsInformation[0].address

        val prms = params ?: throw IllegalArgumentException("Params must not be null")


        val rz1 = Rz(Params(phi + Math.PI / 2, null, null), unitsInformation[0] as Qubit).getLowOperator()
        val rx = Rx(Params(mu = Math.PI / 2, phi = null, theta = null), unitsInformation[0] as Qubit).getLowOperator()
        val rz2 = Rz(Params(mu - Math.PI / 2, null, null), unitsInformation[0] as Qubit).getLowOperator()

        return rz1 * rx * rz2
    }
}