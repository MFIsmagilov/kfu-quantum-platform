package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.exceptions.MuParamNotFound
import kpfu.quantum.server.translator.exceptions.PhiParamNotFound
import kpfu.quantum.server.translator.exceptions.ThetaParamNotFound
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

/**
 * Created by maratismagilov on 05.08.2018.
 */
open class U3(params: Params,
              qubits: List<Qubit>
) : QubitOperator(operatorName = OperatorName.U3,
        qubits = qubits,
        params = params) {

    override fun getLowOperator(): LowLevelOperator {

        if (mu == null) throw MuParamNotFound()

        if (phi == null) throw  PhiParamNotFound()

        if (theta == null) throw  ThetaParamNotFound()

        val address = unitsInformation[0].address

        val rz1 = Rz(Params(phi = phi + 3 * Math.PI, theta = null, mu = null), unitsInformation[0] as Qubit).getLowOperator()
        val rx = Rx(Params(mu = Math.PI, theta = null, phi = null), unitsInformation[0] as Qubit).getLowOperator()
        val rz2 = Rz(Params(phi = theta + Math.PI, theta = null, mu = null), unitsInformation[0] as Qubit).getLowOperator()
        val rz3 = Rz(Params(phi = mu, theta = null, mu = null), unitsInformation[0] as Qubit).getLowOperator()

        return rz1 * rx * rz2 * rz3
//        return LowLevelOperator(
//                mutableListOf(
//                        LogicalAddressingCommandFromClient(
//                                CommandTypes.PHASE,
//                                phi + 3 * Math.PI,
//                                LogicalQubitAddressFromClient(address)),
//                        LogicalAddressingCommandFromClient(
//                                CommandTypes.QET,
//                                -Math.PI / 2,
//                                LogicalQubitAddressFromClient(address)),
//                        LogicalAddressingCommandFromClient(
//                                CommandTypes.PHASE,
//                                theta + Math.PI,
//                                LogicalQubitAddressFromClient(address)),
//                        LogicalAddressingCommandFromClient(
//                                CommandTypes.QET,
//                                -Math.PI / 2,
//                                LogicalQubitAddressFromClient(address)),
//                        LogicalAddressingCommandFromClient(
//                                CommandTypes.PHASE,
//                                mu,
//                                LogicalQubitAddressFromClient(address))
//                )
//        )
    }
}