package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.SingleQubitArbitaryOperator
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import org.apache.commons.math3.complex.Complex

class X(qubit: Qubit) : QubitOperator(OperatorName.X, listOf(qubit)) {
    override fun getLowOperator(): LowLevelOperator {
        val q0 = unitsInformation[0] as Qubit
        return SingleQubitArbitaryOperator(
                arrayOf(
                        arrayOf(
                                Complex.ZERO, Complex.ONE
                        ),
                        arrayOf(
                                Complex.ONE, Complex.ZERO
                        )
                ), q0).getLowOperator()
    }

    override fun toString(): String {
        return "X ${unitsInformation[0].address}"
    }
}