package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.QubitOperator
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import org.apache.commons.math3.complex.Complex

/**
 * Created by maratismagilov on 01.09.2018.
 */
abstract class ArbitaryOperator(
        val matrix: Array<Array<Complex>>,
        operatorName: OperatorName,
        qubits: List<Qubit>) : QubitOperator(
        operatorName = operatorName,
        qubits = qubits) {

//    abstract var alpha: Double
//    abstract var beta: Double
//    abstract var delta: Double
//    abstract var gamma: Double

    override fun getLowOperator(): LowLevelOperator {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}