package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary

import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.Array2DRowComplexMatrix
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator.ControlledSingleQubitOperator
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import org.apache.commons.math3.complex.Complex


class MultiQubitArbitaryOperator(
        matrix: Array<Array<Complex>>,
        qubits: List<Qubit>,
        operatorName: OperatorName = OperatorName.MultiQubitArbitaryOperator
) : ArbitaryOperator(
        operatorName = operatorName,
        matrix = matrix,
        qubits = qubits
) {


    override fun getLowOperator(): LowLevelOperator {

        return Array2DRowComplexMatrix(matrix)
                .decompositionIntoTwoLevelMatricesContainedOnlyNontrivialElement()
                .asSequence()
                .map {
                    ControlledSingleQubitOperator(it.nontrivialElements, this.unitsInformation, this.unitsInformation.size * 2)
                }
                .map {
                    it.decomposeOnCnotAndSingleQubitOperator()
                }
                .flatten()
                .map {
                    it.getLowOperator()
                }
                .reduce { acc, lowLevelOperator ->
                    acc * lowLevelOperator
                }
    }

}