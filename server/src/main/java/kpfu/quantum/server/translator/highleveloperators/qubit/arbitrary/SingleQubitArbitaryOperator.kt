package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary

import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.Ry
import kpfu.quantum.server.translator.highleveloperators.qubit.Rz
import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.LUDecomposition
import org.apache.commons.math3.linear.ArrayRealVector
import kotlin.math.acos
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.sin


/**
 * Created by maratismagilov on 01.09.2018.
 */
class SingleQubitArbitaryOperator(
        matrix: Array<Array<Complex>>,
        qubits: Qubit,
        operatorName: OperatorName = OperatorName.SingleQubitArbitaryOperator
) : ArbitaryOperator(
        matrix = matrix,
        qubits = listOf(qubits),
        operatorName = operatorName) {
    var alpha: Double = 0.0
    var beta: Double = 0.0
    var delta: Double = 0.0
    var gamma: Double = 0.0

    override fun getLowOperator(): LowLevelOperator {
        findParams()
        return Rz(Params(phi = beta, mu = null, theta = null), unitsInformation[0] as Qubit).getLowOperator() *
                Ry(Params(null, null, gamma), unitsInformation[0] as Qubit).getLowOperator() *
                Rz(Params(delta, null, null), unitsInformation[0] as Qubit).getLowOperator()

    }

    /**
     * Decompose controlled single unitary operator
     * @return Return operators from basis
     */
    fun getLowOperator(operator: HighLevelOperator): LowLevelOperator {
        val ho = getControlledOperator(operator)
        val iterator = ho.iterator()
        var accumulator = iterator.next().getLowOperator()
        while (iterator.hasNext()) {
            accumulator *= iterator.next().getLowOperator()
        }
        return accumulator
    }

    /**
     *  U = A{param}B{param}C
     */
    fun getControlledOperator(operator: HighLevelOperator): List<HighLevelOperator> {
        findParams()
        val A = listOf(
                Rz(Params(phi = beta, mu = null, theta = null), unitsInformation[0] as Qubit),
                Ry(Params(phi = null, mu = null, theta = gamma / 2), unitsInformation[0] as Qubit)
        )

        val B = listOf(
                Ry(Params(phi = null, mu = null, theta = -gamma / 2), unitsInformation[0] as Qubit),
                Rz(Params(phi = -(delta + beta) / 2, mu = null, theta = null), unitsInformation[0] as Qubit)
        )

//        println("delta = $delta beta = $beta")

        val C = Rz(Params(phi = (delta - beta) / 2, mu = null, theta = null), unitsInformation[0] as Qubit)


        return mutableListOf<HighLevelOperator>().apply {
            addAll(A)
            add(operator)
            addAll(B)
            add(operator)
            add(C)
        }
    }

    fun findParams() {
        gamma = 2 * Math.acos(matrix[0][0].real)

        val coefficients = Array2DRowRealMatrix(
                arrayOf(
//                        doubleArrayOf(1.0, -1.0 / 2.0, -1.0 / 2.0),
                        doubleArrayOf(1.0, 1.0 / 2.0, -1.0 / 2.0),
                        doubleArrayOf(1.0, -1.0 / 2.0, 1.0 / 2.0),
                        doubleArrayOf(1.0, 1.0 / 2.0, 1.0 / 2.0)),
                false)
        val solver = LUDecomposition(coefficients).solver

        //fixme  acos(matrix[1][0].real / sin(gamma / 2) = 1.0000000000000007

        val constantsReal = ArrayRealVector(
                doubleArrayOf(
//                        matrix[0][1].real,
                        acos(matrix[1][0].real / sin(gamma / 2)),
                        acos(-matrix[0][1].real / sin(gamma / 2)),
                        acos(matrix[1][1].real / cos(gamma / 2))
                ),
                false)
        val solutionReal = solver.solve(constantsReal)

        val constantsImaginary = ArrayRealVector(
                doubleArrayOf(
                        asin(matrix[1][0].imaginary / sin(gamma / 2)),
                        -asin(matrix[0][1].imaginary / sin(gamma / 2)),
                        asin(matrix[1][1].imaginary / cos(gamma / 2))
//                        matrix[0][0].imaginary,
//                        matrix[0][1].imaginary,
//                        matrix[1][0].imaginary,
//                        matrix[1][1].imaginary
                ),
                false)
        val solutionImaginary = solver.solve(constantsImaginary)

//        println("ALPHA BETA DELTA")
//        println(solutionReal.toArray().joinToString(" "))
//        println(solutionImaginary.toArray().joinToString(" "))
        val result = solutionReal.toArray().zip(solutionImaginary.toArray(), Double::plus).toTypedArray()
//        println(result.joinToString(" "))
//        println("!ALPHA BETA DELTA")
//        println()
        alpha = result[0]
        beta = result[1]
        delta = result[2]
//        println("$alpha \n $beta \n $delta \n $gamma")
    }
}