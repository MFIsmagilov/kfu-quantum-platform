package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import kpfu.quantum.server.array2dOf
import org.apache.commons.math3.Field
import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.linear.*
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.round

/**
 * Created by maratismagilov on 01.09.2018.
 */
//todo: use SparseMatrix
//fixme: rename to MultiQubitMatrixOperator
open class Array2DRowComplexMatrix : Array2DRowFieldMatrix<Complex> {

    companion object {

        fun createUnitMatrix(rowDimension: Int, columnDimension: Int): Array2DRowComplexMatrix {
            val matrix = array2dOf(rowDimension, columnDimension) { Complex.ZERO }
            for (i in 0 until rowDimension) {
                for (j in 0 until columnDimension) {
                    if (i == j) {
                        matrix[i][j] = Complex.ONE
                    }
                }
            }
            return Array2DRowComplexMatrix(matrix)
        }

        fun createZeroMatrix(rowDimension: Int, columnDimension: Int): Array2DRowComplexMatrix {
            val matrix = array2dOf(rowDimension, columnDimension) { Complex.ZERO }
            return Array2DRowComplexMatrix(matrix)
        }


    }

    constructor(matrix: Array<Array<Complex>>) : super(matrix)

    constructor(matrix: Field<Complex>, rowDimension: Int, columnDimension: Int) :
            super(array2dOf<Complex>(rowDimension, columnDimension) { Complex.NaN })

    constructor(d: Array<Array<Complex>>, copyArray: Boolean) :
            super(AbstractFieldMatrix.extractField<Complex>(d), d, copyArray)

    constructor(field: Field<Complex>, d: Array<Array<Complex>>, copyArray: Boolean) :
            super(field, d, copyArray)

    override fun createMatrix(rowDimension: Int, columnDimension: Int): FieldMatrix<Complex> {
        return Array2DRowComplexMatrix(field, rowDimension, columnDimension);
    }

    override fun copy(): Array2DRowComplexMatrix {
        return Array2DRowComplexMatrix(copyOut())
    }


    /**
     * @return Return Hermitian matrix
     */
    fun conjugateTranspose(): Array2DRowComplexMatrix {
        val matrix = this.transpose() as Array2DRowComplexMatrix
        for (i in 0 until matrix.rowDimension) {
            for (j in 0 until matrix.columnDimension) {
                matrix.setEntry(i, j, matrix.getEntry(i, j).conjugate())
            }
        }
        return matrix
    }

    /**
     * @return Return Invertible matrix
     */
    fun inverse(): Array2DRowComplexMatrix {
        val solver = FieldLUDecomposition<Complex>(this).solver
        return Array2DRowComplexMatrix(solver.inverse.data)
    }

    fun multiply(m: Array2DRowComplexMatrix): Array2DRowComplexMatrix {
        //кажется это плоховато
        return Array2DRowComplexMatrix(field, super.multiply(m).dataRef, false)
    }

    fun isUnitaryMatrix(): Boolean {
        return conjugateTranspose().multiply(this).isUnitMatrix()
    }

    fun isUnitMatrix(): Boolean {
        for (i in 0 until rowDimension) {
            for (j in 0 until columnDimension) {
                val value = getEntry(i, j)
                return if (i == j) {
                    if (value == Complex.ONE || (round(value.real) == 1.0 && round(value.imaginary) == 0.0))
                        continue
                    else
                        false
                } else {
                    if (value == Complex.ZERO || (round(abs(value.real)) == 0.0 && round(abs(value.imaginary)) == 0.0))
                        continue
                    else
                        return false
                }
            }
        }
        return true
    }

    /**
     * Create new matrix
     *  [              [
     *   [ a, b ]        [value, 0, 0]
     *   [ c, d ]    =   [0, a, b]
     *  ]                [0, c, d]
     *                 ]
     */
    fun addValueToLeftTopAngle(value: Complex = Complex.ONE): Array2DRowComplexMatrix {
        val countRow = rowDimension + 1
        val countCol = columnDimension + 1
        val newMatrix = createZeroMatrix(countRow, countCol)
        newMatrix.setEntry(0, 0, value)
        newMatrix.setSubMatrix(data, 1, 1)
        return newMatrix
    }


    /**
     * Decomposition unitary matrix on composition unitary matrices
     *
     * @return Return list of unitary matrix [U, U1, U2, ... , Un], last element is original matrix
     */
    fun decompositionIntoTwoLevelMatrices(): List<Array2DRowComplexMatrix> {
        if (rowDimension <= 2 && columnDimension <= 2) {
            return listOf(this)
        }
        var tempMatrix = copy()//.inverse()
        val tempVi = mutableListOf<Array2DRowComplexMatrix>()
        tempVi.add(this)

        // Vn-1 * ... * V1 * U = I
        var k = 1 // считаем матрицу V1
        val indexColumn = 0
        for (i in 0 until rowDimension - 1) {
            tempMatrix.apply {

                if (getEntry(i + 1, indexColumn) == Complex.ZERO) {
                    val matrixVk = createUnitMatrix(rowDimension, columnDimension)
                    if (k != 1) {
                        matrixVk.setEntry(0, 0, getEntry(0, 0))
                    }

                    tempVi.add(matrixVk)
                    //tempVi = [U, V1, V2, V3]
                    tempMatrix = tempVi.reduceRight { elem, acc ->
                        //V3 * V2
                        //(V3 * V2) * V1
                        //(V3 * V2  * V1) * U
                        acc.multiply(elem)
                    }
                } else {
                    val v = createUnitMatrix(rowDimension, columnDimension)


                    val divisor = Math.sqrt(
                            getEntry(0, 0).abs().pow(2) + getEntry(i + 1, 0).abs().pow(2)
                    )

                    val a = getEntry(0, 0).conjugate().divide(
                            divisor
                    )
                    v.setEntry(0, 0, a)


                    val b = getEntry(i + 1, 0).divide(
                            divisor
                    )
                    v.setEntry(i + 1, 0, b)

                    val c = getEntry(i + 1, 0).conjugate().divide(
                            divisor
                    )
                    v.setEntry(0, i + 1, c)

                    val d = getEntry(0, 0).multiply(-1).divide(
                            divisor
                    )

                    v.setEntry(i + 1, i + 1, d)

                    tempVi.add(v)
                    tempMatrix = tempVi.reduceRight { elem, acc ->
                        acc.multiply(elem)
                    }
                }
                k++
            }
        }

        //U3*U2*U1*U
        //последняя матрица имеет единицу в верхнем левом углу
        //и нули во всех остальных местах первой строки и столбца
        //раскладвыаем ее тоже на произведение унитарных матриц
        //и добавляем в итоговый набор
        tempMatrix
                .getSubMatrix(1, tempMatrix.rowDimension - 1, 1, tempMatrix.columnDimension - 1)
                .let { it as Array2DRowComplexMatrix }
                .decompositionIntoTwoLevelMatrices()
                .asSequence()
                .dropIf(1) { it.count() > 1 } // смотри описание и условие в начале метода
                .map { it.addValueToLeftTopAngle() }
                .also {
                    tempVi.addAll(it)
                }

        return tempVi.toList()
    }


    /**
     * Decomposition unitary matrix on composition unitary matrices
     *
     * @return Return list of unitary matrix [U1, U2, ... , Un],
     * where Ui = [
     *    a b
     *    c d
     * ]
     * @see NontrivialMatrix to understand wtf is Ui
     */
    fun decompositionIntoTwoLevelMatricesContainedOnlyNontrivialElement(): List<NontrivialMatrix> {
        if (rowDimension <= 2 && columnDimension <= 2) {
            return listOf(NontrivialMatrix.create(this))
        }
        var tempMatrix = copy()//.inverse()
        val tempVi = mutableListOf<Array2DRowComplexMatrix>()
        val tempSparseVi = mutableListOf<NontrivialMatrix>()
        tempVi.add(this)

        // Vn-1 * ... * V1 * U = I
        var k = 1 // считаем матрицу V1
        val indexColumn = 0
        for (i in 0 until rowDimension - 1) {
            tempMatrix.apply {
                if (getEntry(i + 1, indexColumn) == Complex.ZERO) {
                    val matrixVk = createUnitMatrix(rowDimension, columnDimension)
                    if (k != 1) {
                        matrixVk.setEntry(0, 0, getEntry(0, 0))
                    }

                    tempVi.add(matrixVk)
                    //tempVi = [U, V1, V2, V3]
                    tempMatrix = tempVi.reduceRight { elem, acc ->
                        //V3 * V2
                        //(V3 * V2) * V1
                        //(V3 * V2  * V1) * U
                        acc.multiply(elem)
                    }
                } else {
                    val v = createUnitMatrix(rowDimension, columnDimension)
                    val nontrivialMatrixBuilder = NontrivialMatrix.Builder()

                    val divisor = Math.sqrt(
                            getEntry(0, 0).abs().pow(2) + getEntry(i + 1, 0).abs().pow(2)
                    )

                    val a = getEntry(0, 0).conjugate().divide(
                            divisor
                    )
                    v.setEntry(0, 0, a)
                    nontrivialMatrixBuilder.setFirstElement(a, 0, 0)

                    val b = getEntry(i + 1, 0).divide(
                            divisor
                    )
                    v.setEntry(i + 1, 0, b)
                    nontrivialMatrixBuilder.setSecondElement(b, i + 1, 0)

                    val c = getEntry(i + 1, 0).conjugate().divide(
                            divisor
                    )
                    v.setEntry(0, i + 1, c)
                    nontrivialMatrixBuilder.setThirdElement(c, 0, i + 1)

                    val d = getEntry(0, 0).multiply(-1).divide(
                            divisor
                    )

                    v.setEntry(i + 1, i + 1, d)
                    nontrivialMatrixBuilder.setFourthElement(d, i + 1, i + 1)

                    tempVi.add(v)
                    tempSparseVi.add(nontrivialMatrixBuilder.build())
                    tempMatrix = tempVi.reduceRight { elem, acc ->
                        acc.multiply(elem)
                    }
                }
                k++
            }
        }

        //U3*U2*U1*U
        //последняя матрица имеет единицу в верхнем левом углу
        //и нули во всех остальных местах первой строки и столбца
        //раскладвыаем ее тоже на произведение унитарных матриц
        //и добавляем в итоговый набор
        tempMatrix
                .getSubMatrix(1, tempMatrix.rowDimension - 1, 1, tempMatrix.columnDimension - 1)
                .let { it as Array2DRowComplexMatrix }
                .decompositionIntoTwoLevelMatricesContainedOnlyNontrivialElement()
                .asSequence()
                .dropIf(1) { it.count() > 1 } // смотри описание и условие в начале метода
                .also {
                    tempSparseVi.addAll(it)
                }

        return tempSparseVi
    }

    private fun copyOut(): Array<Array<Complex>> {
        return this.data
    }

}