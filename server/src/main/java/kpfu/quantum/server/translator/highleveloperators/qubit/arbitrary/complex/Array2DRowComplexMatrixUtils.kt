package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import kpfu.quantum.server.array2dOf
import org.apache.commons.math3.FieldElement
import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.complex.Complex.INF
import org.apache.commons.math3.complex.Complex.NaN
import org.apache.commons.math3.linear.AbstractFieldMatrix
import org.apache.commons.math3.linear.FieldMatrix

fun createZeroMatrix(rowDimension: Int, columnDimension: Int): Array2DRowComplexMatrix {
    val matrix = array2dOf(rowDimension, columnDimension) { Complex.ZERO }
    return Array2DRowComplexMatrix(matrix)
}

fun Array2DRowComplexMatrix.print() {
    this.dataRef.forEach {
        println(it.joinToString(" ") {
            "(${it.real}, ${it.imaginary})"
        })
    }
}

fun Array2DRowComplexMatrix.prettyPrint() {
    this.dataRef.forEach {
        println(it.joinToString(" ") {
            "(${String.format("%.1f", it.real)}, ${String.format("%.1f", it.imaginary)})"
        })
    }
}

fun FieldMatrix<Complex>.prettyPrint() {
    this.data.forEach {
        println(it.joinToString(" ") {
            "(${String.format("%.1f", it.real)}, ${String.format("%.1f", it.imaginary)})"
        })
    }
}

fun Complex.clearMultiply(factor: Int): Complex {
    if (isNaN) {
        return NaN
    }
    if (java.lang.Double.isInfinite(real) || java.lang.Double.isInfinite(imaginary)) {
        return INF
    }
    val newReal = if (real == 0.0) real else real * factor
    val newImaginary = if (imaginary == 0.0) imaginary else imaginary * factor
    return Complex(newReal, newImaginary)
}

public fun <T> Sequence<T>.dropIf(n: Int, condition: (it: Sequence<T>) -> Boolean): Sequence<T> {
    return if (condition(this)) {
        this.drop(1)
    }else{
        this
    }
}