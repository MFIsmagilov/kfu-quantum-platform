package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import kpfu.quantum.server.translator.clientsdata.Qubit
import org.apache.commons.math3.complex.Complex

/**
 * U = [
 *    1 0 0 0
 *    a 1 b 0
 *    0 0 1 0
 *    c 0 d 1
 * ]
 *
 * ~U = [
 *    a b
 *    c d
 * ]
 * where a,b,c,d - Nontrivial Element, contains complex value and
 * index row and col in matrux U
 */

data class NontrivialElement(
        val value: Complex,
        val originalIndexRow: Int,
        val originalIndexCol: Int
//        val qubitsByCnot: List<Qubit>, //кубиты над которым надо сделать CNOT
//        val qubit: Qubit//
)
