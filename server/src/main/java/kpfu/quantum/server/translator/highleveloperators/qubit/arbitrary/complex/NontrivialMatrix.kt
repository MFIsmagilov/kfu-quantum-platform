package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import org.apache.commons.math3.complex.Complex
import java.lang.IllegalArgumentException

/**
 * U = [
 *    1 0 0 0
 *    a 1 b 0
 *    0 0 1 0
 *    c 0 d 1
 * ]
 *
 * ~U = [
 *    a b
 *    c d
 * ]
 * where ~U nontivial matrix
 *
 * @see NontrivialElement to understand wtf is a,b,c,d
 */
open class NontrivialMatrix(
        val nontrivialElements: List<List<NontrivialElement>>
) {

    override fun toString(): String {
        val sb = StringBuilder(4)
        nontrivialElements.forEach {
            it.forEach {
                sb.append(it.toString())
            }
            sb.append("\n")
        }
        return sb.toString()
    }

    companion object {

        fun create(subMatrix: Array2DRowComplexMatrix, originalSize: Int = -1): NontrivialMatrix {

            if (subMatrix.columnDimension > 2 || subMatrix.rowDimension > 2) {
                throw IllegalArgumentException("Matrix can be only 2x2")
            }

            val a = NontrivialElement(subMatrix.getEntry(0, 0), 0, 0)
            val b = NontrivialElement(subMatrix.getEntry(0, 1), 0, 1)
            val c = NontrivialElement(subMatrix.getEntry(1, 0), 1, 0)
            val d = NontrivialElement(subMatrix.getEntry(1, 1), 1, 1)


            return NontrivialMatrix(
                    listOf(
                            listOf(a, b),
                            listOf(c, d)
                    )
            )
        }

    }


    class Builder {
        lateinit var a: NontrivialElement
        lateinit var b: NontrivialElement
        lateinit var c: NontrivialElement
        lateinit var d: NontrivialElement

        fun setFirstElement(complex: Complex, i: Int, j: Int) {
            a = NontrivialElement(complex, i, j)
        }

        fun setSecondElement(complex: Complex, i: Int, j: Int) {
            b = NontrivialElement(complex, i, j)
        }

        fun setThirdElement(complex: Complex, i: Int, j: Int) {
            c = NontrivialElement(complex, i, j)
        }

        fun setFourthElement(complex: Complex, i: Int, j: Int) {
            d = NontrivialElement(complex, i, j)
        }

        fun build(): NontrivialMatrix {
            return NontrivialMatrix(
                    listOf(
                            listOf(a, b),
                            listOf(c, d)
                    )
            )
        }
    }
}