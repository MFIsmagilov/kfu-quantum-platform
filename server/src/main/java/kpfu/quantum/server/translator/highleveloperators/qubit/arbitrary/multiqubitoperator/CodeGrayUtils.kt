package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator

import java.lang.Integer.max

fun Int.getBit(position: Int): Int {
    return this shr position and 1
}

fun Int.reverseBit(position: Int): Int {
    return this xor (1 shl position)
}

fun Int.toBin(): String {
    return this.toString(radix = 2)
}

/**
 * Build list of gray code between s and t
 * @example s =41, t = 51, result = [
 *  101001
 *  101011
 *  100011
 *  110011
 * ]
 * @return List of integer, which if convert to binary is gray code
 */
fun getCodesGray(s: Int, t: Int): List<Int> {
    var nextGrayCode = s
    var bitPosition = 0
    val codes = mutableListOf(nextGrayCode)

    while (nextGrayCode != t) {
        if (nextGrayCode.getBit(bitPosition) == t.getBit(bitPosition)) {
            bitPosition++
        } else {
            nextGrayCode = nextGrayCode.reverseBit(bitPosition)
            bitPosition++
            codes.add(nextGrayCode)
        }
    }
    return codes
}

/**
 *
 */
@Deprecated("")
fun getQubitNumbers(firstGrayCode: Int, secondGrayCode: Int) {

    val firstSize = firstGrayCode.toBin().length
    val secondSize = secondGrayCode.toBin().length

    val size = max(firstSize, secondSize)

    var targetQubit = -1
    val controlQubits = mutableListOf<Int>()
    for (i in 0 until size) {
        if (firstGrayCode.getBit(i) != secondGrayCode.getBit(i)) {
            targetQubit = i
        }
    }

}

fun main(args: Array<String>) {


    getCodesGray(1, 6).forEach {
        println(it.toBin())
    }
//    println(41.toBin())
//    for (i in 0..7) {
//        println(41.getBit(i))
//    }
//    println()
//
//    getCodesGray(41, 51).forEach {
//        println(it.toBin())
//    }
}