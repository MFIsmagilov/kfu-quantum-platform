package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.clientsdata.UnitInformation
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.CCNOT
import kpfu.quantum.server.translator.highleveloperators.qubit.CNOT
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.SingleQubitArbitaryOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.NontrivialElement
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.NontrivialMatrix
import kpfu.quantum.server.translator.highleveloperators.qubit.X

/**
 *
 * U = [
 *        1 0 0 0
 *        0 a 0 b
 *        0 0 1 0
 *        0 c 0 d
 * ]
 *
 * @see NontrivialMatrix we decompose on NontrivialMatrix, which are ControlledSingleQubitOperator
 * originalCountQubit
 *
 * Count qubits: n = log2(U.colSize or U.rowSize) as U is square matrix
 * n - 1 : additional qubits which user don't know
 * @param originalQubit ~ n
 * @param additionalQubit ~ n - 1
 *
 */

class ControlledSingleQubitOperator(nontrivialElements: List<List<NontrivialElement>>,
                                    val originalQubits: List<UnitInformation>,
                                    val additionalCountQubit: Int) : NontrivialMatrix(nontrivialElements) {

    fun decomposeOnCnotAndSingleQubitOperator(): MutableList<HighLevelOperator> {

        if (originalQubits.size < 2) throw IllegalArgumentException("This operator should be contained more qubits")

        //fixme because flatten create new collection
        if (nontrivialElements.flatten().size != 4) throw IllegalArgumentException("Dude! Operator can be contains four nontrivial elements!")

        //да-да за два прохода находим min и max, почему бы и нет
        val min = nontrivialElements.asSequence().flatten().minBy { it.originalIndexCol }!!.originalIndexCol
        val max = nontrivialElements.asSequence().flatten().maxBy { it.originalIndexCol }!!.originalIndexCol

        val graySequence = getCodesGray(min, max)
        val qubitNum = getQubitNumbers(graySequence, originalQubits.size).also {
            println("<Номера кубитов над которыми надо сделать CCNOT>")
            it.forEach {
                println(it)
            }
            println("</Номера кубитов над которыми надо сделать CCNOT>")
        }.toMutableList()

        val additionNumberQubit = (1..additionalCountQubit).map { it + originalQubits.size }


        val chain = mutableListOf<HighLevelOperator>()

        var idxCurrentGrayCode = 0
        var currentGrayCode = graySequence[idxCurrentGrayCode]
        val countBits = getCountBitsByGrayCode(graySequence)


        qubitNum.forEachIndexed { index, controlledQubit ->

            val operatorsX = getXoperators(graySequence, currentGrayCode, controlledQubit, originalQubits.size)
            chain.addAll(operatorsX)
            currentGrayCode = graySequence[++idxCurrentGrayCode]

            val operatorsForReverse = mutableListOf<HighLevelOperator>() //для возращения базиса

            var indexUsedAdditionQubit = 0
            val controlabledNumberQubits = originalQubits.filter { it.address != controlledQubit }.toMutableList()


            if (controlabledNumberQubits.size == 1) {
                val targetQubit = Qubit.create(controlledQubit)
                val cnot = CNOT(controlabledNumberQubits[0] as Qubit, targetQubit)
                chain.addAll(getSingleQubitOperator(targetQubit).getControlledOperator(cnot))
            } else if (controlabledNumberQubits.size == 2) {
                val qubit1 = controlabledNumberQubits[0] as Qubit
                val qubit2 = controlabledNumberQubits[1] as Qubit
                val qubit3 = Qubit.create(controlledQubit)
                val ccnot = CCNOT(qubit1, qubit2, qubit3)
                chain.add(ccnot)
            } else {
                val qubit1 = controlabledNumberQubits[0] as Qubit
                val qubit2 = controlabledNumberQubits[1] as Qubit
                val unsedContrilableNumberQubits = controlabledNumberQubits.drop(2)
                val qubit3 = Qubit.create(additionNumberQubit[indexUsedAdditionQubit])
                val firstCCNOT = CCNOT(qubit1, qubit2, qubit3)
                chain.add(firstCCNOT)

                if (unsedContrilableNumberQubits.isNotEmpty()) {
                    operatorsForReverse.add(firstCCNOT)
                    var qubit = qubit3
                    for (idxControlableNumberQubit in 0 until unsedContrilableNumberQubits.size - 1) {
                        val controlableNumberQuibit = unsedContrilableNumberQubits[idxControlableNumberQubit]
                        indexUsedAdditionQubit++
                        val newControlledQubit = Qubit.create(additionNumberQubit[indexUsedAdditionQubit])
                        val ccnot = CCNOT(qubit, controlableNumberQuibit as Qubit, newControlledQubit)
                        chain.add(ccnot)
                        operatorsForReverse.add(ccnot)
                        qubit = newControlledQubit
                    }

                    if (index == qubitNum.size - 1) {

                        val controlableNumberQubit = additionNumberQubit[++indexUsedAdditionQubit]
                        val qubitControled = Qubit.create(controlableNumberQubit)
                        val ccnot = CCNOT(qubit, unsedContrilableNumberQubits.last() as Qubit, qubitControled)
                        chain.add(ccnot)
                        val targetQubit = Qubit.create(controlledQubit)
                        val cnot = CNOT(qubitControled, targetQubit)
                        chain.addAll(getSingleQubitOperator(targetQubit).getControlledOperator(cnot))

                    } else {
                        val controlableNumberQubit = unsedContrilableNumberQubits.last() as Qubit
                        val ccnot = CCNOT(qubit, controlableNumberQubit, Qubit.create(controlledQubit))
                        chain.add(ccnot)
                        chain.addAll(operatorsForReverse.reversed())
                        chain.addAll(operatorsX)
                    }
                }
            }
        }
        return chain
    }


    /**
     * Get number of qubit for operator C..CNOT
     * @param grayCodeSequence sequence of code gray, for example
     * [
     *    00001
     *    00000
     *    00010
     *    00110
     *    01110
     * ]
     * For 00001 and 00000 is 5
     * For 00000 and 00010 is 4
     * For 00010 and 00110 is 3
     * For 00110 and 01110 is 2
     *
     * @return list of number of qubit, for example [5,4,3,2]
     */
    fun getQubitNumbers(grayCodeSequence: List<Int>, countQubits: Int): List<Int> {
        val countBits = getCountBitsByGrayCode(grayCodeSequence)
        val result = mutableListOf<Int>()
        for (i in 0..grayCodeSequence.size - 2) {
            for (bitNumber in 0 until countBits) {
                if (grayCodeSequence[i].getBit(bitNumber) != grayCodeSequence[i + 1].getBit(bitNumber)) {
                    result.add(i, countQubits - bitNumber)
                }
            }
        }

        return result
    }

    fun getCountBitsByGrayCode(grayCodeSequence: List<Int>): Int {
        return grayCodeSequence.max()?.toBin()?.length
                ?: throw IllegalArgumentException("Not found max element in gray code sequence")
    }

    /**
     * Creating single qubit operator to get A*CNOT*B*CNOT*C
     */
    private fun getSingleQubitOperator(qubit: Qubit): SingleQubitArbitaryOperator {
        val complex2dArray = nontrivialElements
                .map { it.map { it.value }.toTypedArray() }
                .toTypedArray()
        return SingleQubitArbitaryOperator(complex2dArray, qubit)
    }

    /**
     * For example get 00001 and 00000, excluded number qubit is 5, because
     *               ___________________
     * #qubit       | 1 | 2 | 3 | 4 | 5 |
     *              |___|___|___|___|___|
     * current      | 0 | 0 | 0 | 0 | 1 |
     * gray row     |___|___|___|___|___|
     *              | 0 | 0 | 0 | 0 | 0 |
     * next row     |___|___|___|___|___|
     *
     * @return list of number qubit on which to execute X operator, for example
     * 1, 2, 3, 4, because bit contained zero
     * @param grayCodeSequence sequence of code gray, for example
     *       [
     *           00001
     *           00000
     *           00010
     *           00110
     *           01110
     *       ]
     * @param excludeNumberQubit - address/number of qubit on which  to execute the target CCNOT
     */
    fun getXoperators(graySequence: List<Int>, currentGrayCode: Int, excludeNumberQubit: Int, countQubits: Int): List<HighLevelOperator> {
        val operators = mutableListOf<HighLevelOperator>()
        val countBits = getCountBitsByGrayCode(graySequence)
        for (i in 0..countBits) {
            if (countQubits - i != excludeNumberQubit && currentGrayCode.getBit(i) == 0) {
                operators.add(X(Qubit.create(countQubits - i)))
            }
        }
        return operators
    }
}