package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.clientsdata.UnitInformation
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.CNOT
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.SingleQubitArbitaryOperator
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.NontrivialElement
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.NontrivialMatrix
import kpfu.quantum.server.translator.highleveloperators.qubit.tweakmathmodel.MCCNOT


//fixme сделать абстрактный класс SingleQubitOperator и два наследника чтобы не дублировать код

class QuickControlledSingleQubitOperator(nontrivialElements: List<List<NontrivialElement>>,
                                         val originalQubits: List<UnitInformation>,
                                         val additionalCountQubit: Int) : NontrivialMatrix(nontrivialElements) {

    fun decomposeOnCnotAndSingleQubitOperator(): MutableList<HighLevelOperator> {

        if (originalQubits.size < 2) throw IllegalArgumentException("This operator should be contained more qubits")

        //fixme because flatten create new collection
        if (nontrivialElements.flatten().size != 4) throw IllegalArgumentException("Dude! Operator can be contains four nontrivial elements!")

        //да-да за два прохода находим min и max, почему бы и нет
        val min = nontrivialElements.asSequence().flatten().minBy { it.originalIndexCol }!!.originalIndexCol
        val max = nontrivialElements.asSequence().flatten().maxBy { it.originalIndexCol }!!.originalIndexCol

        val graySequence = getCodesGray(min, max)
        val qubitNum = getQubitNumbers(graySequence, originalQubits.size).also {
            println("<Номера кубитов над которыми надо сделать CCNOT>")
            it.forEach {
                println(it)
            }
            println("</Номера кубитов над которыми надо сделать CCNOT>")
        }.toMutableList()

        val additionNumberQubit = (1..additionalCountQubit).map { it + originalQubits.size }


        val chain = mutableListOf<HighLevelOperator>()

        var idxCurrentGrayCode = 0
        var currentGrayCode = graySequence[idxCurrentGrayCode]
        val countBits = getCountBitsByGrayCode(graySequence)


        qubitNum.forEachIndexed { index, controlledQubit ->

            val qubitForX = getQubitNumbersForX(graySequence, currentGrayCode, controlledQubit, originalQubits.size)
            currentGrayCode = graySequence[++idxCurrentGrayCode]

            val operatorsForReverse = mutableListOf<HighLevelOperator>() //для возращения базиса

            var indexUsedAdditionQubit = 0
            val controlabledNumberQubits = originalQubits.filter { it.address != controlledQubit }.toMutableList()


            if (controlabledNumberQubits.size == 1) {
                val targetQubit = Qubit.create(controlledQubit)
                val cnot = CNOT(controlabledNumberQubits[0] as Qubit, targetQubit)
                chain.addAll(getSingleQubitOperator(targetQubit).getControlledOperator(cnot))
            } else if (controlabledNumberQubits.size == 2) {
                val qubit1 = controlabledNumberQubits[0] as Qubit
                val qubit2 = controlabledNumberQubits[1] as Qubit
                val qubit3 = Qubit.create(controlledQubit)
                val ccnot = getMCCNOT(qubitForX, qubit1, qubit2, qubit3)
                chain.add(ccnot)
            } else {
                val qubit1 = controlabledNumberQubits[0] as Qubit
                val qubit2 = controlabledNumberQubits[1] as Qubit
                val unsedContrilableNumberQubits = controlabledNumberQubits.drop(2)
                val qubit3 = Qubit.create(additionNumberQubit[indexUsedAdditionQubit])
                val firstCCNOT = getMCCNOT(qubitForX, qubit1, qubit2, qubit3)
                chain.add(firstCCNOT)

                if (unsedContrilableNumberQubits.isNotEmpty()) {
                    operatorsForReverse.add(firstCCNOT)
                    var qubit = qubit3
                    for (idxControlableNumberQubit in 0 until unsedContrilableNumberQubits.size - 1) {
                        val controlableNumberQuibit = unsedContrilableNumberQubits[idxControlableNumberQubit]
                        indexUsedAdditionQubit++
                        val newControlledQubit = Qubit.create(additionNumberQubit[indexUsedAdditionQubit])
                        val ccnot = getMCCNOT(qubitForX, qubit, controlableNumberQuibit as Qubit, newControlledQubit)
                        chain.add(ccnot)
                        operatorsForReverse.add(ccnot)
                        qubit = newControlledQubit
                    }

                    if (index == qubitNum.size - 1) {

                        val controlableNumberQubit = additionNumberQubit[++indexUsedAdditionQubit]
                        val qubitControled = Qubit.create(controlableNumberQubit)
                        val ccnot = getMCCNOT(qubitForX, qubit, unsedContrilableNumberQubits.last() as Qubit, qubitControled)
                        chain.add(ccnot)
                        val targetQubit = Qubit.create(controlledQubit)
                        val cnot = CNOT(qubitControled, targetQubit)
                        chain.addAll(getSingleQubitOperator(targetQubit).getControlledOperator(cnot))

                    } else {
                        val controlableNumberQubit = unsedContrilableNumberQubits.last() as Qubit
                        val ccnot = getMCCNOT(qubitForX, qubit, controlableNumberQubit, Qubit.create(controlledQubit))
                        chain.add(ccnot)
                        chain.addAll(operatorsForReverse.reversed())
                    }
                }
            }
        }
        return chain
    }


    /**
     * Get number of qubit for operator C..CNOT
     * @param grayCodeSequence sequence of code gray, for example
     * [
     *    00001
     *    00000
     *    00010
     *    00110
     *    01110
     * ]
     * For 00001 and 00000 is 5
     * For 00000 and 00010 is 4
     * For 00010 and 00110 is 3
     * For 00110 and 01110 is 2
     *
     * @return list of number of qubit, for example [5,4,3,2]
     */
    fun getQubitNumbers(grayCodeSequence: List<Int>, countQubits: Int): List<Int> {
        val countBits = getCountBitsByGrayCode(grayCodeSequence)
        val result = mutableListOf<Int>()
        for (i in 0..grayCodeSequence.size - 2) {
            for (bitNumber in 0 until countBits) {
                if (grayCodeSequence[i].getBit(bitNumber) != grayCodeSequence[i + 1].getBit(bitNumber)) {
                    result.add(i, countQubits - bitNumber)
                }
            }
        }

        return result
    }

    fun getCountBitsByGrayCode(grayCodeSequence: List<Int>): Int {
        return grayCodeSequence.max()?.toBin()?.length
                ?: throw IllegalArgumentException("Not found max element in gray code sequence")
    }

    /**
     * Creating single qubit operator to get A*CNOT*B*CNOT*C
     */
    private fun getSingleQubitOperator(qubit: Qubit): SingleQubitArbitaryOperator {
        val complex2dArray = nontrivialElements
                .map { it.map { it.value }.toTypedArray() }
                .toTypedArray()
        return SingleQubitArbitaryOperator(complex2dArray, qubit)
    }

    fun getQubitNumbersForX(graySequence: List<Int>, currentGrayCode: Int, excludeNumberQubit: Int, countQubits: Int): List<Int> {
        //fixme use hashSet because need quick contains function
        val qubitNumbers = mutableListOf<Int>()
        val countBits = getCountBitsByGrayCode(graySequence)
        for (i in 0..countBits) {
            if (countQubits - i != excludeNumberQubit && currentGrayCode.getBit(i) == 0) {
                qubitNumbers.add(countQubits - i)
            }
        }
        return qubitNumbers
    }


    fun getMCCNOT(qubitsForX: List<Int>, qubit1: Qubit, qubit2: Qubit, qubit3: Qubit): MCCNOT {
        return MCCNOT(qubit1, qubit2, qubit3, qubitsForX.contains(qubit1.address), qubitsForX.contains(qubit2.address))
    }
}