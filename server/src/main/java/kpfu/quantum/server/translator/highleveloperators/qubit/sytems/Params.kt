package kpfu.quantum.server.translator.highleveloperators.qubit.sytems

/**
 * Created by maratismagilov on 05.08.2018.
 */
//fixme избавиться от Params и в deserializers просто парсить и создавать операторы которым нужны параметры
//fixme кажется что вводить это было плохое решение, гляди на кудиты @see QuditOperatorDeserializer
class Params(
        val phi: Double?,
        val mu: Double?,
        val theta: Double?
){
    companion object {
        fun empty() = Params(0.0, 0.0, 0.0)
    }
}