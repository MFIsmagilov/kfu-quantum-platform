package kpfu.quantum.server.translator.highleveloperators.qubit.tweakmathmodel

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.quantum.server.translator.clientsdata.Qubit

class CCNOT110(qubit1: Qubit, qubit2: Qubit, qubit3: Qubit) : TweakCCNOT(qubit1, qubit2, qubit3) {
    override fun getCommandType() = CommandTypes.CQET110

    override fun getName() = "CCNOT100"
}
