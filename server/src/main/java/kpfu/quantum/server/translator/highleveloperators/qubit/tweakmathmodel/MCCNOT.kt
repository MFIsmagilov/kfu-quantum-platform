package kpfu.quantum.server.translator.highleveloperators.qubit.tweakmathmodel

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.quantum.server.translator.clientsdata.Qubit

class MCCNOT(qubit1: Qubit, qubit2: Qubit, qubit3: Qubit, val valueCq1: Boolean, val valuewCq2: Boolean) : TweakCCNOT(qubit1, qubit2, qubit3) {
    override fun getCommandType(): CommandTypes {
        return when (valueCq1) {
            true -> when (valuewCq2) {
                true -> CommandTypes.CQET110
                false -> CommandTypes.CQET100
            }
            false -> when (valuewCq2) {
                true -> CommandTypes.CQET010
                false -> CommandTypes.CQET000
            }
        }
    }

    override fun getName() = "MCCNOT"
}