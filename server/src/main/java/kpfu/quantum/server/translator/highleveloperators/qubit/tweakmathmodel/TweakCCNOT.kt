package kpfu.quantum.server.translator.highleveloperators.qubit.tweakmathmodel

import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qubit.QubitLowLevelOperator

abstract class TweakCCNOT(qubit1: Qubit, qubit2: Qubit, qubit3: Qubit) : HighLevelOperator(OperatorName.TWEKABLEOPERATOR, listOf(qubit1, qubit2, qubit3)) {
    override fun getLowOperator(): LowLevelOperator {
        val q1 = unitsInformation[0].address
        val q2 = unitsInformation[1].address
        val q3 = unitsInformation[2].address
        return QubitLowLevelOperator(
                mutableListOf(
                        LogicalAddressingCommandFromClient(getCommandType(),
                                getParam(),
                                LogicalQubitAddressFromClient(q1),
                                LogicalQubitAddressFromClient(q2),
                                LogicalQubitAddressFromClient(q3))
                )
        )
    }

    abstract fun getCommandType(): CommandTypes

    abstract fun getName(): String

    open fun getParam(): Double = Math.PI

    override fun toString(): String {
        return "${getName()} ${unitsInformation[0].address} ${unitsInformation[1].address} ${unitsInformation[2].address}"
    }
}