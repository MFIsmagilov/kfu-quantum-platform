package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.ApplyX
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator
import kotlin.math.sqrt

class ApplyF0(val idxQMem: Long, val dim: Long) : QuditOperator(OperatorName.ApplyF0) {
    override fun getLowOperator(): LowLevelOperator {
        val instructions = mutableListOf<LogicalAddressingCommandFromClient>()
        for (i in 1 until dim) {
            val b = sqrt((dim - i).toDouble() / dim.toDouble())
            val ladcfc = LogicalAddressingCommandFromClient(
                    ApplyX(idxQMem, i, sqrt(1.0 / dim.toDouble()), b))
            instructions.add(ladcfc)
        }
        return QuditLowLevelOperator(instructions)
    }
}
