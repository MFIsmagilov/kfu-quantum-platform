package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.ApplyXConjugate
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

class ApplyXConjWrap(
        val idxQMem: Long,
        val i: Long,
        val x: Double,
        val y: Double
) : QuditOperator(OperatorName.ApplyXConj) {
    override fun getLowOperator(): LowLevelOperator {
        return QuditLowLevelOperator(
                mutableListOf(LogicalAddressingCommandFromClient(
                        ApplyXConjugate(idxQMem, i, x, y)
                ))
        )
    }
}