package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.ApplyX
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

class ApplyXWrap(val idxQmem: Long,
                 val i: Long,
                 val x: Double,
                 val y: Double) : QuditOperator(OperatorName.ApplyX) {
    override fun getLowOperator(): LowLevelOperator {
        return QuditLowLevelOperator(
                mutableListOf(
                        LogicalAddressingCommandFromClient(ApplyX(idxQmem, i, x, y))
                )
        )
    }
}