package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.ApplyZConjugate
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

class ApplyZConjWrap(
        val idxQMem: Long,
        val i: Long,
        val tau: Double

) : QuditOperator(OperatorName.ApplyZConj) {
    override fun getLowOperator(): LowLevelOperator {
        return QuditLowLevelOperator(
                mutableListOf(
                        LogicalAddressingCommandFromClient(
                                ApplyZConjugate(idxQMem, i, tau)
                        )
                )
        )
    }

}