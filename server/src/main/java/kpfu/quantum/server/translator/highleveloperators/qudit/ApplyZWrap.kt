package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.ApplyZ
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

class ApplyZWrap(val idxQMem: Long,
                 val i: Long,
                 val tau: Double) : QuditOperator(OperatorName.ApplyZ) {

    override fun getLowOperator(): LowLevelOperator {

        return QuditLowLevelOperator(
                mutableListOf(
                        LogicalAddressingCommandFromClient(ApplyZ(idxQMem, i, tau))
                )
        )
    }

}