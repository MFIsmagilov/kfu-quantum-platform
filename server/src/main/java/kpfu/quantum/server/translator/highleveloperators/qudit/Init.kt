package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.InitQditStates
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

class Init(val nreg: Long, val dim: Long) : QuditOperator(OperatorName.Init) {


    override fun getLowOperator(): LowLevelOperator {
        return QuditLowLevelOperator(
                mutableListOf(LogicalAddressingCommandFromClient(InitQditStates(nreg, dim)))
        )
    }
}