package kpfu.quantum.server.translator.highleveloperators.qudit

import kmqc.manager.instruction.Measure
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator
import kpfu.quantum.server.translator.lowleveloperators.qudit.LogicalAddressingCommandFromClient
import kpfu.quantum.server.translator.lowleveloperators.qudit.QuditLowLevelOperator

class MeasureWrap(val idxQMem: Int, val idxCMem: Int) : QuditOperator(OperatorName.Measure) {
    override fun getLowOperator(): LowLevelOperator {
        return QuditLowLevelOperator(
                mutableListOf(LogicalAddressingCommandFromClient(
                        Measure(idxQMem, idxCMem)
                ))
        )
    }

}