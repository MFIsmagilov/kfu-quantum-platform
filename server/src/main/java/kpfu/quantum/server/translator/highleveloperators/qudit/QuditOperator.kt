package kpfu.quantum.server.translator.highleveloperators.qudit

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.google.gson.Gson
import kpfu.quantum.server.translator.OperatorName
import kpfu.quantum.server.translator.highleveloperators.HighLevelOperator
import kpfu.quantum.server.translator.clientsdata.deserializers.QuditOperatorDeserializer


/**
 * Немного другая идея, поэтому доп абстракция
 */
//думаю здесь в качестве unitsInformation может служить idxQMem
@JsonDeserialize(using = QuditOperatorDeserializer::class)
abstract class QuditOperator(name:OperatorName): HighLevelOperator(name, listOf(), null){

    override fun toString(): String {
        return Gson().toJson(this)
    }
}