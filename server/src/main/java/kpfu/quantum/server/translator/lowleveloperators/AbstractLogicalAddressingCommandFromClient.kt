package kpfu.quantum.server.translator.lowleveloperators

import com.google.gson.GsonBuilder

abstract class AbstractLogicalAddressingCommandFromClient {
    open fun toJson(): String {
        return GsonBuilder().create().toJson(this)
    }

    override fun toString(): String {
        return toJson()
    }
}