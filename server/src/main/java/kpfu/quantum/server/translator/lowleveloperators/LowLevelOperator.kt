package kpfu.quantum.server.translator.lowleveloperators


abstract class LowLevelOperator(val commands: MutableList<AbstractLogicalAddressingCommandFromClient>) {

    //fixme нормально ли портить текущую коллекцию?
    operator fun times(lowLevelOperator: LowLevelOperator): LowLevelOperator {
        commands.addAll(lowLevelOperator.commands)
        return this
    }

    override fun toString(): String {
        return commands.map { it.toJson() }.joinToString("\n")
    }
}