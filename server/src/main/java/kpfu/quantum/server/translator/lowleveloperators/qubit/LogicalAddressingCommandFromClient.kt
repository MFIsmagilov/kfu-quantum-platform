package kpfu.quantum.server.translator.lowleveloperators.qubit

import com.google.gson.annotations.SerializedName
import kpfu.magistracy.controller.execution.commands.CommandTypes
import kpfu.magistracy.service_for_controller.addresses.LogicalQubitAddressFromClient
import kpfu.quantum.server.translator.lowleveloperators.AbstractLogicalAddressingCommandFromClient

class LogicalAddressingCommandFromClient() : AbstractLogicalAddressingCommandFromClient() {
    //command name from universal basis
    @SerializedName("command_name")
    var commandType: CommandTypes? = null
        private set
    @SerializedName("command_param")
    var commandParam: Double? = null
        private set
    @SerializedName("qubit_1")
    var qubit_1: LogicalQubitAddressFromClient? = null

    @SerializedName("qubit_2")
    var qubit_2: LogicalQubitAddressFromClient? = null

    @SerializedName("qubit_3")
    var qubit_3: LogicalQubitAddressFromClient? = null

    constructor(commandType: CommandTypes, commandParam: Double?, qubit_1: LogicalQubitAddressFromClient, qubit_2: LogicalQubitAddressFromClient, qubit_3: LogicalQubitAddressFromClient) : this() {
        this.commandType = commandType
        this.commandParam = commandParam
        this.qubit_1 = qubit_1
        this.qubit_2 = qubit_2
        this.qubit_3 = qubit_3
    }


    constructor(commandType: CommandTypes, commandParam: Double?, qubit_1: LogicalQubitAddressFromClient, qubit_2: LogicalQubitAddressFromClient) : this() {
        this.commandType = commandType
        this.commandParam = commandParam
        this.qubit_1 = qubit_1
        this.qubit_2 = qubit_2
    }

    constructor(commandType: CommandTypes, commandParam: Double?, qubit_1: LogicalQubitAddressFromClient) : this() {
        this.commandType = commandType
        this.commandParam = commandParam
        this.qubit_1 = qubit_1
    }
}