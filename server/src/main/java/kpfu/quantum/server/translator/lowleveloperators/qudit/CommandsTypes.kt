package kpfu.quantum.server.translator.lowleveloperators.qudit

import com.google.gson.annotations.SerializedName

enum class CommandsTypes {
    @SerializedName("X")
    X,
    @SerializedName("Z")
    Z,
    @SerializedName("F")
    F,

    MEASURE,

    INIT
}