package kpfu.quantum.server.translator.lowleveloperators.qudit

import com.google.gson.annotations.SerializedName
import kmqc.manager.instruction.QInstruction
import kpfu.quantum.server.translator.lowleveloperators.AbstractLogicalAddressingCommandFromClient

//QInstructionWrapper
//больше наследования и оберток
open class LogicalAddressingCommandFromClient(val qInstruction: QInstruction) : AbstractLogicalAddressingCommandFromClient(){

    fun execute(){
        qInstruction.execute()
    }

}