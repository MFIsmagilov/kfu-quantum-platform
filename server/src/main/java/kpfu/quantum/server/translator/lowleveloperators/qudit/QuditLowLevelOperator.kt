package kpfu.quantum.server.translator.lowleveloperators.qudit

import kpfu.quantum.server.translator.lowleveloperators.LowLevelOperator

class QuditLowLevelOperator(commands: MutableList<LogicalAddressingCommandFromClient>) :
        LowLevelOperator(commands.toMutableList()) {

    fun execute() {
        commands.forEach {
            (it as LogicalAddressingCommandFromClient).execute()
        }
    }

}

