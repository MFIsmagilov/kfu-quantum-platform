package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.magistracy.service_for_controller.ResultCallback
import kpfu.quantum.server.translator.clientsdata.HighQuantumProgram
import kpfu.quantum.server.translator.clientsdata.QubitQuantumProgram
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.clientsdata.User
import org.junit.Test


internal class CNOTTest {


    @Test
    fun testCNOT() {
        QubitQuantumProgram(HighQuantumProgram(2,
                arrayListOf(CNOT(Qubit.create(1), Qubit.create(2)))
        ), User("hhh-1001")).execute(ResultCallback { ownerData, result ->
            result.forEach { t, u ->
                println(t.logicalQubitAddress.toString() + " " + u)
            }
        })
    }

    @Test
    fun testMCNOT() {
        QubitQuantumProgram(HighQuantumProgram(2,
                arrayListOf(MCNOT(Qubit.create(1), Qubit.create(2)))
        ), User("hhh-1001")).execute(ResultCallback { ownerData, result ->
            result.forEach { t, u ->
                println(t.logicalQubitAddress.toString() + " " + u)
            }
        })
    }
}