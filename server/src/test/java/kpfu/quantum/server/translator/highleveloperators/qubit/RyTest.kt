package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import org.junit.Test


internal class RyTest {

    @Test
    fun foo() {
        val theta = 3.14
        val ry = Ry(Params(theta = theta, phi = null, mu = null), Qubit.create(1))
        ry.getLowOperator().commands.forEach {
            println((it as LogicalAddressingCommandFromClient).commandType?.name + " " + (it as LogicalAddressingCommandFromClient).commandParam)
        }

        println()


        val first = Rz(Params(theta = null, phi = 3.14 / 2, mu = null), Qubit.create(1)).getLowOperator()
        val secon = Rx(Params(theta = null, phi = null, mu = theta), Qubit.create(1)).getLowOperator()
        val third = Rz(Params(theta = null, phi = -3.14 / 2, mu = null), Qubit.create(1)).getLowOperator()

        (first * secon * third).commands.forEach {
            println((it as LogicalAddressingCommandFromClient).commandType?.name + " " + (it as LogicalAddressingCommandFromClient).commandParam)
        }
    }

}