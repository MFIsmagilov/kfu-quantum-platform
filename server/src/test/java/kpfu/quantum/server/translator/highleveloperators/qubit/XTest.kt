package kpfu.quantum.server.translator.highleveloperators.qubit

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator.getCodesGray
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator.toBin
import kpfu.quantum.server.translator.highleveloperators.qubit.sytems.Params
import org.junit.Test

internal class XTest {

    @Test
    fun getLowOperator() {
        X(Qubit.create(1)).getLowOperator().commands.forEach {
            println(it.toJson())
        }
    }


    @Test
    fun gr() {
        getCodesGray(25,30).forEach {
            println(it.toBin())
        }
    }
}