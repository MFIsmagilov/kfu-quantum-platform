package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.getFourier2dArray
import kpfu.quantum.server.translator.lowleveloperators.qubit.LogicalAddressingCommandFromClient
import org.junit.Test


class MultiQubitArbitaryOperatorTest {


    @Test
    fun foo() {
        val low = MultiQubitArbitaryOperator(getFourier2dArray(),

                listOf(
                        Qubit.create(1),
                        Qubit.create(2),
                        Qubit.create(3),
                        Qubit.create(4)
                )
        ).getLowOperator()


        println("Количество операторов " + low.commands.size)

        low.commands.forEach {
            (it as LogicalAddressingCommandFromClient).apply {
                println(it.commandType)
            }
        }
    }


    @Test
    fun bar() {
        val low = QuickMultiQubitArbitaryOperator(getFourier2dArray(),

                listOf(
                        Qubit.create(1),
                        Qubit.create(2),
                        Qubit.create(3),
                        Qubit.create(4)
                )
        ).getLowOperator()


        println("Количество операторов " + low.commands.size)

        low.commands.forEach {
            (it as LogicalAddressingCommandFromClient).apply {
                println(it.commandType)
            }
        }
    }

}