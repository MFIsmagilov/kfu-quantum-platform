package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import org.apache.commons.math3.complex.Complex
import org.junit.Test

class AddedColumnAndRowTest {
    @Test
    fun `test added row and column`() {

        //processing
        val matrix = getFourierMatrix()
        println("Original matrix")
        matrix.prettyPrint()
        println("After added padding")
        val padMatrix = matrix.addValueToLeftTopAngle()
        padMatrix.prettyPrint()
        println()


        //result
        val matrixResult = Array2DRowComplexMatrix(Array2DRowComplexMatrix(arrayOf(
                arrayOf(Complex.ZERO, Complex.ZERO, Complex.ZERO, Complex.ZERO, Complex.ZERO),
                arrayOf(Complex.ZERO, Complex.ONE, Complex.ONE, Complex.ONE, Complex.ONE),
                arrayOf(Complex.ZERO, Complex.ONE, Complex(0.0, 1.0), Complex(-1.0, 0.0), Complex(0.0, -1.0)),
                arrayOf(Complex.ZERO, Complex.ONE, Complex(-1.0, 0.0), Complex(1.0, 0.0), Complex(-1.0, 0.0)),
                arrayOf(Complex.ZERO, Complex.ONE, Complex(0.0, -1.0), Complex(-1.0, 0.0), Complex(0.0, 1.0))

        )).scalarMultiply(Complex(0.5, 0.0)).data)
        matrixResult.setEntry(0, 0, Complex.ONE)
        println("Expected matrix")
        matrixResult.prettyPrint()

        println("Check")
        padMatrix.equals(matrixResult).also {
            println(it)
            assert(it)
        }
    }

    @Test
    fun foo() {
        val l = listOf(1, 2, 3, 4, 5, 6, 7)

        l.reduceRight { i, acc ->
            println("$i $acc")
            i + acc
        }

        l.reduceRightIndexed { index, i, acc ->
            println("$index $i $acc")
            acc + i
        }
    }
}