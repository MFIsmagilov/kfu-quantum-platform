package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.linear.Array2DRowFieldMatrix
import org.junit.Assert
import org.junit.Test

open class Array2DRowComplexMatrixTest {

    @Test
    fun `test conjugate transpose for matrix`() {
        val matrix = Array2DRowComplexMatrix(getTestMatrix()).conjugateTranspose()
        Assert.assertEquals(matrix, getResultMatrix())
    }

    fun getTestMatrix(): Array<Array<Complex>> {
        return arrayOf(
                arrayOf(
                        Complex(1.0, 0.0), Complex(-2.0, -1.0), Complex(5.0, 0.0)
                ),
                arrayOf(
                        Complex(1.0, 1.0), Complex(0.0, 1.0), Complex(4.0, -2.0)
                )
        )
    }

    fun getResultMatrix(): Array2DRowComplexMatrix {
        return Array2DRowComplexMatrix(arrayOf(
                arrayOf(
                        Complex(1.0, -0.0), Complex(1.0, -1.0)
                ),
                arrayOf(
                        Complex(-2.0, 1.0), Complex(0.0, -1.0)
                ),
                arrayOf(
                        Complex(5.0, -0.0), Complex(4.0, 2.0)
                )
        ))
    }

    @Test
    fun `check that matrices U_i is unitary after matrix decomposition U`() {
        getFourierMatrix().let {
            it.decompositionIntoTwoLevelMatrices().forEachIndexed { index, array2DRowFieldMatrix ->
                println("Check for U${if (index == 0) "" else index}")
                array2DRowFieldMatrix.prettyPrint()
                println()
                assert(array2DRowFieldMatrix.isUnitaryMatrix())
            }
        }
    }

    @Test
    fun `print nontrival result for decomposition U`() {
        getFourierMatrix().let {
            val str = it.decompositionIntoTwoLevelMatricesContainedOnlyNontrivialElement().joinToString("\n")
            println(str)
        }
    }

    @Test
    fun `check that Un * Un-1 * ? * U1 * inverse(U) = I for Fourier Matrix( etc`() {
        val u = getFourierMatrix().decompositionIntoTwoLevelMatrices().asReversed()
        val lastIndex = u.size - 1
        u
                .reduceIndexed { index, acc, matrix ->
                    if (index == lastIndex) //original matrix need inverse
                        matrix.inverse().multiply(acc)
                    else
                        matrix.multiply(acc)
                }
                .isUnitMatrix()
                .also {
                    println("Un * Un-1 * ? * U1 * inverse(U) = I is $it")
                    assert(it)
                }
    }

    @Test
    fun `check that U = Un * Un-1 * ? * U1 for Fourier Matrix( etc`() {
        //проверять на глаз :) ибо 0.0 != -0.0
        val u = getFourierMatrix().decompositionIntoTwoLevelMatrices().asReversed()
        println("Original matrix = ")
        getFourierMatrix().prettyPrint()
        println()

        val ui = u.dropLast(1)

        ui.reduce { acc, matrix ->
            matrix.inverse().multiply(acc)
        }.apply {
            println("Un * Un-1 * ... * U1 = ")
            this.prettyPrint()
        }
    }

    @Test
    fun `check that decompositionIntoTwoLevelMatrices method not return U4 U3 U2 U1 U for Fourier Matrix( etc`() {
        getFourierMatrix().let { operator ->
            //проверим только что последняя матрица в списке оригнальная
            assert(operator.decompositionIntoTwoLevelMatrices().last() != operator)
        }
    }

    @Test
    fun `check that decompositionIntoTwoLevelMatrices method return U U1 U2 U3 U4 for Fourier Matrix( etc`() {
        getFourierMatrix().let { operator ->
            //проверим только что последняя матрица в списке оригнальная
            assert(operator.decompositionIntoTwoLevelMatrices().first() == operator)
        }
    }


    @Test
    fun `check (U^T)* multiply U = I= `() {
        getFourierMatrix().decompositionIntoTwoLevelMatricesContainedOnlyNontrivialElement().forEachIndexed { index, nontrivialMatrix ->

            val arr = nontrivialMatrix.nontrivialElements.map {
                it.map {
                    it.value
                }
            }.map {
                it.toTypedArray()
            }.toTypedArray()

            val u = Array2DRowComplexMatrix(arr)
            val uu = Array2DRowComplexMatrix(arr).conjugateTranspose().multiply(u)

            println("#$index")
            uu.print()
            println()
        }
    }
}