package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import org.apache.commons.math3.complex.Complex
import org.junit.Test

class Array2DRowComplexMatrixTestBooleanMethods {


    @Test
    fun `test#1 method which create unit matrix and check that matrix is unit matrix`() {
        assert(getUnitMatrix().isUnitMatrix())
    }

    @Test
    fun `test#2 method which create unit matrix and check that matrix is unit matrix`() {
        assert(!getMatrix().isUnitMatrix())
    }


    @Test
    fun `test#3 method which check that matrix is unitary matrix`() {
        assert(getUnitaryMatrix().isUnitaryMatrix())
    }

    private fun getUnitMatrix(): Array2DRowComplexMatrix {
        return Array2DRowComplexMatrix.createUnitMatrix(10, 10)
    }

    private fun getMatrix(): Array2DRowComplexMatrix {
        return Array2DRowComplexMatrix.createUnitMatrix(10, 10).apply {
            setEntry(8, 8, Complex.I)
        }
    }

    private fun getUnitaryMatrix(): Array2DRowComplexMatrix {
        return Array2DRowComplexMatrix(Array2DRowComplexMatrix(arrayOf(
                arrayOf(Complex.ONE, Complex.ONE, Complex.ONE, Complex.ONE),
                arrayOf(Complex.ONE, Complex(0.0, 1.0), Complex(-1.0, 0.0), Complex(0.0, -1.0)),
                arrayOf(Complex.ONE, Complex(-1.0, 0.0), Complex(1.0, 0.0), Complex(-1.0, 0.0)),
                arrayOf(Complex.ONE, Complex(0.0, -1.0), Complex(-1.0, 0.0), Complex(0.0, 1.0))

        )).scalarMultiply(Complex(0.5, 0.0)).data)
    }
}