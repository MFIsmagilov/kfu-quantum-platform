package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.complex.ComplexField
import org.apache.commons.math3.linear.SparseFieldMatrix
import org.junit.Assert
import org.junit.Test

class ExtensionsTest {

    @Test
    fun `test dropIf extension`() {
        val list = listOf(1, 2, 3, 4, 5, 6, 7)

        list
                .asSequence()
//                .map { it * 2 }
                .dropIf(1) {
                    println(it.count())
                    it.count() == 7
                }
                .toList()
                .apply {
                    Assert.assertArrayEquals(this.toTypedArray(), arrayOf(2, 3, 4, 5, 6, 7))
                }
                .joinToString(" ")
                .also {
                    println(it)
                }
    }

    @Test
    fun foo(){
        val sm = SparseFieldMatrix<Complex>(ComplexField.getInstance(), 4,4)
        sm.setEntry(2,2, Complex.ONE)

        val sm1 = SparseFieldMatrix<Complex>(ComplexField.getInstance(), 4,4)
        sm1.setEntry(1,1, Complex.ONE)
        sm.add(sm1).prettyPrint()

    }
}