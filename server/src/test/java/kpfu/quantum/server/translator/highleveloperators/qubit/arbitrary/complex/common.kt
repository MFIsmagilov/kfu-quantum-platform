package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex

import org.apache.commons.math3.complex.Complex

fun getFourierMatrix(): Array2DRowComplexMatrix {
    return Array2DRowComplexMatrix(Array2DRowComplexMatrix(arrayOf(
            arrayOf(Complex.ONE, Complex.ONE, Complex.ONE, Complex.ONE),
            arrayOf(Complex.ONE, Complex(0.0, 1.0), Complex(-1.0, 0.0), Complex(0.0, -1.0)),
            arrayOf(Complex.ONE, Complex(-1.0, 0.0), Complex(1.0, 0.0), Complex(-1.0, 0.0)),
            arrayOf(Complex.ONE, Complex(0.0, -1.0), Complex(-1.0, 0.0), Complex(0.0, 1.0))

    )).scalarMultiply(Complex(0.5, 0.0)).data)
}

fun getFourier2dArray(): Array<Array<Complex>> {
    return getFourierMatrix().dataRef
}