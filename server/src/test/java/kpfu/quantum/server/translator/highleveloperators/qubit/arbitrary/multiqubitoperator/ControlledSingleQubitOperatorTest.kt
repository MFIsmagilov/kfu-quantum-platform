package kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.multiqubitoperator

import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.NontrivialElement
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.getFourierMatrix
import kpfu.quantum.server.translator.highleveloperators.qubit.arbitrary.complex.prettyPrint
import org.apache.commons.math3.complex.Complex
import org.junit.Assert
import org.junit.Test

internal class ControlledSingleQubitOperatorTest {


    @Test
    fun bar(){
        getFourierMatrix().decompositionIntoTwoLevelMatrices().map { println(it.prettyPrint()) }
    }

    @Test
    fun foo(){
        getFourierMatrix().decompositionIntoTwoLevelMatricesContainedOnlyNontrivialElement()

                .map {
                    ControlledSingleQubitOperator(it.nontrivialElements, listOf( Qubit.create(1), Qubit.create(2)), 2)
                            .decomposeOnCnotAndSingleQubitOperator()
                }
                .flatten()
                .map {
                    println(it.getLowOperator())
                }
//                .map {
//                    println(it)
//                }

    }

    @Test
    fun decomposeOnCnotAndSingleQubitOperator() {

        val csqo = ControlledSingleQubitOperator(getNontrivialElements(), listOf(
                Qubit.create(1),
                Qubit.create(2),
                Qubit.create(3),
                Qubit.create(4),
                Qubit.create(5)), 4)

        csqo.decomposeOnCnotAndSingleQubitOperator().forEach {
            println(it)
        }

    }

    @Test
    fun getQubitNumbers() {
        val csqo = ControlledSingleQubitOperator(getNontrivialElements(), listOf(
                Qubit.create(1),
                Qubit.create(2),
                Qubit.create(3),
                Qubit.create(4),
                Qubit.create(5)), 4) // stub

        val qubitsNumber = csqo.getQubitNumbers(getCodesGray(1, 14), 5)
        qubitsNumber.forEach {
            println(it.toString())
        }
        Assert.assertArrayEquals(qubitsNumber.toTypedArray(), arrayOf(5, 4, 3, 2))
    }

    @Test
    fun setXoperator() {
        val csqo = ControlledSingleQubitOperator(getNontrivialElements(), listOf(
                Qubit.create(1),
                Qubit.create(2),
                Qubit.create(3),
                Qubit.create(4),
                Qubit.create(5)), 4) // stub
        csqo.getXoperators(getCodesGray(1, 14), 6, 2, 5).forEach {
            println((it.unitsInformation[0] as Qubit).address)
        }
    }

    private fun getNontrivialElements(): List<List<NontrivialElement>> {
        return listOf(
                listOf(
                        NontrivialElement(Complex.ONE, 1, 1),
                        NontrivialElement(Complex.ONE, 1, 14)
                ),
                listOf(
                        NontrivialElement(Complex.ONE, 14, 1),
                        NontrivialElement(Complex.ONE, 14, 14)
                )
        )
    }
}