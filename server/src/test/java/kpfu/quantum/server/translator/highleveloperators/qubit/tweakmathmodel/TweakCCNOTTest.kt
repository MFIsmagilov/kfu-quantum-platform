package kpfu.quantum.server.translator.highleveloperators.qubit.tweakmathmodel

import kpfu.magistracy.service_for_controller.ResultCallback
import kpfu.quantum.server.translator.clientsdata.HighQuantumProgram
import kpfu.quantum.server.translator.clientsdata.QubitQuantumProgram
import kpfu.quantum.server.translator.clientsdata.Qubit
import kpfu.quantum.server.translator.clientsdata.User
import org.junit.Test


internal class TweakCCNOTTest {

    @Test
    fun `test CCNOT000`() {


        QubitQuantumProgram(
                HighQuantumProgram(
                        4,
                        arrayListOf(CCNOT000(Qubit.create(1), Qubit.create(2), Qubit.create(3)))
                ),
                User("hhh-000-1")
        ).execute(ResultCallback { ownerData, result ->
            result.forEach { t, u ->
                println(t.logicalQubitAddress.toString() + " " + u)
            }
        })

    }

    @Test
    fun `test CCNOT110`() {


        QubitQuantumProgram(
                HighQuantumProgram(
                        4,
                        arrayListOf(CCNOT110(Qubit.create(1), Qubit.create(2), Qubit.create(3)))
                ),
                User("hhh-000-1")
        ).execute(ResultCallback { ownerData, result ->
            result.forEach { t, u ->
                println(t.logicalQubitAddress.toString() + " " + u)
            }
        })

    }
}